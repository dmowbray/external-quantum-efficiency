from setuptools import setup

setup(name='eqe',
      version='24.01',
      description='External Quantum Efficiency Calculator',
      maintainer='Duncan John Mowbray',
      maintainer_email='duncan.mowbray@gmail.com',
      url='https://gitlab.com/dmowbray/external-quantum-efficiency',
      platforms=['unix'],
      license='GPLv3+',
      scripts=['bin/eqe',
               'tests/carbon_chain.py',
               'tests/tight_binding.py',
               'tests/graphene.py'],
      packages=['eqe', 'eqe.tests', 'eqe.transport'])
