#!/usr/bin/env python

"""Test of tight binding implementation of
ExternalQuantumEfficiency class
using the EQE of 8x8 <nabla> = 0.1 system"""

from os.path import exists
from os import chdir, mkdir

from eqe.tests import TestEQE, read_arguments

def main():
    """Tight-Binding 8x8 <nabla> = 0.1 calculation"""
    dname = 'tight_binding'
    if not exists('tmp'):
        mkdir('tmp')
    chdir('tmp')
    if not exists(dname):
        mkdir(dname)
    chdir(dname)
    # Propagation and polarization in the 'z' direction
    args = read_arguments()
    results = {}
    results[2.01] = 0.30156652689077973
    results[3] = 3.049414933398862e-06
    args = read_arguments()
    test = TestEQE(filename=None,
                   mode=args.mode,
                   results=results,
                   error=args.error,
                   pdir=0, qdir=0)
    if args.full:
        test.eqe.plot_transmission(usemax=False)

main()
