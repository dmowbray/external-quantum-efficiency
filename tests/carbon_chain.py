#!/usr/bin/env python

"""Test for spin polarization dependence of
ExternalQuantumEfficiency class
using the EQE of a carbon chain"""
from os.path import exists
from os import chdir, mkdir

from ase import Atoms
from gpaw import GPAW, FermiDirac

from eqe.tests import TestEQE, read_arguments

def main():
    """C6 Chain EQE test
    LDA, SZP, 5 Ang. Vacuum d_C-C = 1.3375 Ang."""
    xcf = 'LDA'
    basis = 'dzp'
    dname = 'carbon_chain'
    name = dname+'_'+xcf+'_'+basis+'.gpw'
    if not exists('tmp'):
        mkdir('tmp')
    chdir('tmp')
    if not exists(dname):
        mkdir(dname)
    chdir(dname)
    if not exists(name):
        # Isolated C atom
        atoms = Atoms('C')
        carbon_d = 1.3375
        vacuum = 10
        natoms = 12
        atoms.set_pbc(True)
        cell = [carbon_d, vacuum, vacuum]
        atoms.set_cell(cell)
        atoms.center()
        atoms = atoms.repeat([natoms,1,1])
        # Orthogonal 6 carbon atom chain
        atoms.set_initial_magnetic_moments([4 for a in atoms])
        calc = GPAW(mode='lcao',
                    xc=xcf,
                    h=0.2,
                    basis=basis,
                    occupations=FermiDirac(width=0.001),
                    kpts=[1,1,1])
        atoms.set_calculator(calc)
        atoms.get_potential_energy()
        calc.write(name, mode='all')
        del calc
    args = read_arguments()
    # Propogation and polarization in the 'x' direction
    qdir = 0
    pdir = 0
    results = {}
    spin = 0
    results = {}
    results[0.01] = 0.6428242555884647
    #results[2] = 0.007962872847119256
    results[2] = 0.0013810321776390438
    test = TestEQE(filename=name,
                   mode=args.mode,
                   results=results,
                   pdir=pdir,
                   qdir=qdir,)
    spin = 1
    results = {}
    results[0.01] = 0.032218842489934785
    #results[2] = 0.0028693351234603762
    results[2] = 0.0011161073998940914
    test.check_results(results=results, spin=spin)

    # Polarization in y direction
    qdir = 1
    spin = 0
    results = {}
    results[0.01] = 5.076411372262335e-06
    #results[2] = 5.155239054393183e-08
    results[2.0] = 2.613626631046195e-08
    test = TestEQE(filename=name,
                   mode=args.mode,
                   results=results,
                   pdir=pdir,
                   qdir=qdir,)
    spin = 1
    results = {}
    results[0.01] = 3.925522462644437e-09
    results[2] = 4.5556526312277296e-11
    test.check_results(results=results, spin=spin)

main()
