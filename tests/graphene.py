#!/usr/bin/env python

"""Test for perpendicular k-point dependence of
ExternalQuantumEfficiency class
using the EQE of graphene with few perendicular k-points"""
from os.path import exists
from os import chdir, mkdir

from numpy import sqrt

from ase import Atoms
from gpaw import GPAW, FermiDirac
from eqe.tests import TestEQE, read_arguments

def main():
    """Graphene EQE test
    LDA, SZP, 5 Ang. Vacuum d_C-C = 1.41 Ang."""
    xcf = 'LDA'
    basis = 'szp(dzp)'
    dname = 'graphene'
    name = dname+'_'+xcf+'_'+basis+'.gpw'
    if not exists('tmp'):
        mkdir('tmp')
    chdir('tmp')
    if not exists(dname):
        mkdir(dname)
    chdir(dname)
    if not exists(name):
        # Isolated C atom
        atoms = Atoms('C')
        carbon_d = 1.41
        vacuum = 10
        atoms.set_pbc(True)
        cell = [carbon_d, carbon_d, vacuum]
        atoms.set_cell(cell)
        atoms = atoms.repeat([2,1,1])
        # Pair of C atoms
        cell = [[3*carbon_d/2., sqrt(3)*carbon_d/2., 0],
                [3*carbon_d/2., -sqrt(3)*carbon_d/2., 0],
                [0, 0, vacuum]]
        atoms.set_cell(cell)
        atoms = atoms.repeat([2,1,1])
        cell = [3*carbon_d, sqrt(3)*carbon_d, vacuum]
        atoms.set_cell(cell)
        atoms.center()
        # Orthogonal Graphene 4C atoms
        atoms = atoms.repeat([6,1,1])
        calc = GPAW(mode='lcao',
                    xc=xcf,
                    h=0.2,
                    basis=basis,
                    occupations=FermiDirac(width=0.001),
                    kpts=[1,7,1])
        atoms.set_calculator(calc)
        atoms.get_potential_energy()
        calc.write(name, mode='all')
        del calc
    args = read_arguments()
    results = {}
    results[2.9] = 0.023075155442878185
    results[2] = 0.0008636734662643703
    results[3] = 0.0007159637871358305
    results[4] = 0.003255006118669304
    test = TestEQE(filename=name,
                   mode=args.mode,
                   results=results,
                   pdir=0,
                   qdir=0,
                   error=args.error)
    if args.full:
        test.eqe.plot_transmission(usemax=True)

main()
