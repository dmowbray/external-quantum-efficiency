#!/usr/bin/env python

"""This module defines the EQE class
for calculating the External Quantum Efficiency
based on a GPAW gpw Hamiltonian in LCAO mode or
a Tight-Binding Hamiltonian

EQE calculations are embarassingly parallel via
separate frequency and k-point dependent output files,
so all calculations should be submitted as single-processes.
Multithreading is employed significantly throughout as
the bottle-necks are matrix inversions.
"""

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division


from time import sleep
from os.path import exists
from os import chdir, mkdir, listdir

from argparse import ArgumentParser

from numpy import zeros, arange, trapz,\
array, pi, exp, sqrt, sort, ones
from numpy.linalg import eigvals

from matplotlib.tri import Triangulation
from matplotlib.colors import LinearSegmentedColormap
import pylab as p

from ase.units import Ha
from ase.parallel import parprint

from eqe.transport import ListOfSlices, LeftLeadHamiltonianMatrix, \
    RightLeadHamiltonianMatrix, ScatteringHamiltonianMatrix, TransmissionTool

from eqe.hamiltonian_matrices import TBMatrices, LCAOMatrices

def read_arguments():
    """Input Argument Parsing"""

    parser = ArgumentParser()
    parser.add_argument('-f', '--filename',
                        help="name of the input GPAW file with LCAO hamiltonian",
                        default=None, type=str)
    parser.add_argument('-v', '--verbose',
                        help='increase output verbosity',
                        action='store_true')
    parser.add_argument('-tb', '--tightbinding',
                        help='use a tight-binding hamiltonian',
                        action='store_true')
    parser.add_argument('-Egap', '--energygap',
                        help='tight-binding band gap (%(default)s eV)',
                        default=1., type=float)
    parser.add_argument('-nnc', '--nncoupling',
                        help='tight-binding nearest-neighbour coupling energy (%(default)s eV)',
                        default=0.25, type=float)
    parser.add_argument('-n', '--nabla',
                        help='expectation value of nabla operator <nabla> (%(default)s a.u.)',
                        default=0.1, type=float)
    parser.add_argument('-vol', '--volume',
                        help='tight binding unit cell volume (%(default)s a0^3)',
                        default=8*pi, type=float)
    parser.add_argument('-M', '--size',
                        help='tighbinding matrix size (%(default)s)',
                        default=8, type=int)
    parser.add_argument('-wmin', '--omegamin',
                        help='energy range minimum (%(default)s eV)',
                        default=0.01, type=float)
    parser.add_argument('-wmax', '--omegamax',
                        help='energy range maximum (%(default)s eV)',
                        default=4., type=float)
    parser.add_argument('-dw', '--domega',
                        help='energy increment (%(default)s eV)',
                        default=0.01, type=float)
    parser.add_argument('-dvb', '--dvb',
                        help='valence band energy increment (%(default)s eV)',
                        default=0.01, type=float)
    parser.add_argument('-qd', '--qdir',
                        help='polarization direction (%(default)s)',
                        default=2, type=int)
    parser.add_argument('-pd', '--pdir',
                        help='propagation direction for transmission (%(default)s)',
                        default=2, type=int)
    parser.add_argument('-ddc', '--scissor',
                        help='scissors correction (%(default)s eV)',
                        default=0, type=float)
    parser.add_argument('-eta', '--eta',
                        help='transmission electronic broadening (%(default)s eV)',
                        default=1e-5, type=float)
    parser.add_argument('-eta_dos', '--eta_dos',
                        help='density of states broadening (%(default)s eV)',
                        default=1e-5, type=float)
    parser.add_argument('-z', '--addzeros',
                        help='add zeros to transmission for plotting',
                        action='store_true')
    parser.add_argument('-p', '--plot',
                        help='plot absorption and transmission probability',
                        action='store_true')
    parser.add_argument('-tmax', '--transmax',
                        help='maximum of transmission contour plot (%(default)s)',
                        default=1.0, type=float)
    parser.add_argument('-umax', '--usemax',
                        help='use transmission maximum for contour plot (%(default)s)',
                        action='store_true')
    parser.add_argument('-rdos', '--read_dos',
                        help='read DOS from output files (%(default)s)',
                        action='store_true')
    parser.add_argument('-eff', '--efficient',
                        help='optimize calculations for efficiency (%(default)s)',
                        action='store_true')
    parser.add_argument('-dbg', '--debug',
                        help='perform debugging checks (%(default)s)',
                        action='store_false')
    parser.add_argument('-unocc', '--calc_unocc_dos',
                        help='calculate unoccupied density of states (%(default)s)',
                        action='store_true')
    pargs = parser.parse_args()
    assert pargs.qdir in [0, 1, 2]
    assert pargs.pdir in [0, 1, 2]
    return pargs


class ExternalQuantumEfficiency(object):
    """External Quantum Efficiency Calculator Class"""

    def __init__(self, filename=None, verbose=False, tightbinding=False,
                 size=8, energygap=1.0, coupling=0.25, nabla=0.1, volume=8*pi,
                 wmin=None, wmax=4., domega=0.01, dvb=0.01, qdir=2, pdir=2,
                 eta=1e-5, eta_dos=1e-5, scissor=0.,
                 efficient=False, debug=True, calc_unocc_dos=False, frequencies=None):
        """External Quantum Efficiency Calculator
        filename	GPAW LCAO DFT gpw file
        verbose		verbosity (False)
        tightbinding	Use a tight-binding halmitonian (False)
        size		number of onsite elements in tight-binding matrix (8)
	energygap	Energy gap for TB model (1 eV)
	coupling	nearest-neighbour coupling, 1/4 band width, (0.25 eV)
	nabla		expectation values of nabla <nabla> (0.1 a.u.)
        volume		tight-binding unit cell volume (8 pi a0^3)
        wmin		minimum excitation energy (domega eV)
        wmax		maximum excitation energy (4 eV)
        domega	        excitation energy step size (0.01 eV)
        qdir		polarization direction (2)
	pdir		propagation direction (2)
	eta		transmission electronic width (1e-5 eV)
	eta_dos		density of states broadening (1e-5 eV)
        scissor		derivative discontinuity correction (0 eV)
        efficient	simplify calculations for optimized efficiency (False)
        debug		debug checking (assertions) and verbose printing (True)
        calc_unocc_dos	calculate and output unoccupied densitiy of states (False)
        frequencies	array of excitation frequencies in eV (None)"""
        self.matrices = self.initialize_matrices(qdir=qdir,
                                                 pdir=pdir,
                                                 filename=filename,
                                                 tightbinding=tightbinding,
                                                 size=size,
                                                 energygap=energygap,
                                                 coupling=coupling,
                                                 nabla=nabla,
                                                 volume=volume)
        if wmin is None:
            wmin = domega
        self.left = 0
        self.right = 1
        self.verbose = verbose
        self.debug = debug
        self.efficient = efficient
        self.calc_unocc_dos = calc_unocc_dos
        self.dirs = ['x', 'y', 'z']
        # For more efficient calculations, only calculate EQE
        # for frequencies above the band gap
        if efficient:
            wmin = max(wmin, self.matrices.energygap-domega)
        # Check that wmin is a multiple of domega
        wmin = int(wmin/domega)*domega
        self.wmin = wmin
        self.domega = domega
        self.dvb = dvb
        if frequencies is None:
            self.frequencies = arange(wmin, wmax+domega-scissor, domega)
        else:
            self.frequencies = array(frequencies)
        self.efficiencies = zeros([self.matrices.nspins,
                                   len(self.frequencies)])
        self.verboseprint('Frequencies', self.frequencies)
        self.all_energies = arange(-self.frequencies[-1], -dvb/2, dvb)
        self.total_dos = zeros(len(self.all_energies))
        #self.verboseprint('Energies', self.all_energies)
        self.outfilename = self.get_outfilename(filename, tightbinding)
        self.postpend = '.dat'
        self.eta = eta
        self.eta_dos = eta_dos
        self.scissor = scissor
        self.delay = 1 # Delay to wait for file to finish writing
        self.verboseprint(self)
        self.calculated = False # calculation flag

    def verboseprint(self, *args, **kwargs):
        """MPI-safe verbose print
        Prints only from master if verbose is True."""

        if self.verbose:
            parprint(*args, **kwargs)

    def __repr__(self):
        """String Representation for External Quantum Efficiency Class"""
        strrep = str(self.matrices)
        strrep += ", ωmin=%.3f eV" % self.frequencies[0]
        strrep += ", ωmax=%.3f eV" % self.frequencies[-1]
        strrep += ", Δω=%.3f eV" % (self.frequencies[1]-self.frequencies[0])
        strrep += ", η=%.5f eV" % self.eta
        strrep += ", ηDOS=%.5f eV" % self.eta_dos
        strrep += ", Δxc=%.3f eV" % self.scissor
        strrep += ", T=%s s" % self.delay
        return strrep

    def get_outfilename(self, filename, tightbinding=False):
        """Define output file name
        filename	GPAW gpw file name
	tightbinding	Tightbinding (False)"""
        if tightbinding:
            outputfilename = "TightBinding"
        else:
            outputfilename = filename.split('.gpw')[0]
        return outputfilename

    def initialize_matrices(self, qdir, pdir, filename, tightbinding,
                            size, energygap, coupling, nabla, volume):
        """Initialize Hamiltonian, Overlap, and Interaction matrices
        qdir		polarization direction
	pdir		propagation direction
        filename	GPAW LCAO DFT gpw file
        tightbinding	Use a tight-binding halmitonian
        size		number of onsite elements in tight-binding matrix
	energygap	Energy gap for TB model
	coupling	level coupling, 1/4 band width
	nabla		expectation value of nabla <nabla>
        volume		tight-binding unit cell volume in a0^3"""

        if tightbinding:
            # Generate Tight-Binding Matrix Elements
            matrices = TBMatrices(qdir=qdir,
                                  pdir=pdir,
                                  size=size,
                                  energygap=energygap,
                                  coupling=coupling,
                                  nabla=nabla,
                                  volume=volume)
        elif filename is not None:
            # Read DFT Matrix Elements from LCAO Calculation
            matrices = LCAOMatrices(filename=filename,
                                    qdir=qdir,
                                    pdir=pdir)
        else:
            raise ValueError("Missing GPAW filename")

        return matrices

    def setup_leads(self, h_lead, s_lead, end, omega=0):
        """Setup Leads for EQE Calculation"""
        leads = []
        prin = int(h_lead.shape[0]/2)
        couplingindices = ListOfSlices([slice(prin, 2*prin), slice(-2*prin, -prin)])
        leads.append(LeftLeadHamiltonianMatrix(principallayer=prin,
                                               hamiltonianmatrix=h_lead,
                                               overlapmatrix=s_lead,
                                               leadindices=0,
                                               couplingindices=couplingindices))
        leads.append(RightLeadHamiltonianMatrix(principallayer=prin,
                                                hamiltonianmatrix=h_lead-omega*s_lead,
                                                overlapmatrix=s_lead,
                                                leadindices=-prin,
                                                couplingindices=couplingindices))
        for lead in leads:
            lead.GetLeadIndices().WrapNegativeSlices(end)
        return leads, couplingindices

    def __energy_to_str(self, energy):
        """Convert an energy to a string with appropriate
        decimal places based on domega
        energy	energy value in eV"""
        domega = min(self.domega, self.dvb)
        num_decimals = str(domega)[::-1].find('.')
        str_format = '%.'+str(num_decimals)+'f'
        return str_format % energy

    def calculate_dos(self, h_scatt, s_scatt, h_lead, s_lead, energies):
        """Calculate Density of States in scattering region
        using NEGF formalism
        h_scatt		Scattering Region Hamiltonian
        s_scatt		Scattering Region Overlaps
	h_lead		Lead Hamiltonians
	s_lead		Lead Overlaps"""
        prin = int(h_lead.shape[0]/2)
        couplingindices = ListOfSlices([slice(prin, 2*prin)])
        leftlead = LeftLeadHamiltonianMatrix(principallayer=prin,
                                             hamiltonianmatrix=h_lead,
                                             overlapmatrix=s_lead,
                                             leadindices=0,
                                             couplingindices=couplingindices)
        rightlead = RightLeadHamiltonianMatrix(principallayer=prin,
                                               hamiltonianmatrix=h_lead,
                                               overlapmatrix=s_lead,
                                               leadindices=-prin,
                                               couplingindices=couplingindices)
        hmat = ScatteringHamiltonianMatrix(leftprincipallayer=prin,
                                           rightprincipallayer=prin,
                                           hamiltonianmatrix=h_scatt,
                                           overlapmatrix=s_scatt,
                                           leftlead=leftlead,
                                           rightlead=rightlead,
                                           scattering=couplingindices)
        leads = []
        leads.append(leftlead)
        leads.append(rightlead)
        left = 0
        right = 1
        trans = TransmissionTool(hmat, leads[left], leads[right], energies, prin)
        trans.InitMultiLeadScatteringGreenFunction(leads, left, right)
        leadwidth = self.eta_dos
        trans.GetLeftLeadGreenFunction().SetInfinitesimal(leadwidth)
        trans.GetRightLeadGreenFunction().SetInfinitesimal(leadwidth)
        width = self.eta_dos
        trans.GetScatteringGreenFunction().SetInfinitesimal(width)
        #trans.GetScatteringGreenFunction().WideBandLimitOn()
        trans.UpdateTransmission()
        trans.GetScatteringGreenFunction().ReferenceGreenFunctionOn()
        trans.GetScatteringGreenFunction().ReferenceSelfEnergiesOn()
        for energy in energies:
            filename = self.__ofilename('dos_w', energy=energy)
            if not exists(filename):
                ofile = self.__ofilehandle('dos_w', energy=energy)
                trans.GetScatteringGreenFunction().SetEnergy(energy)
                print(self.__energy_to_str(energy),
                      abs(trans.GetDensityOfStates()),
                      file=ofile)
                ofile.close()

    def read_dos(self, energies):
        """Read in DOS from txt files"""
        dos = []
        delay = self.delay
        for energy in energies:
            rfile = self.__ofilehandle('dos_w', energy, read=True)
            data = rfile.readline()
            while not data:
                sleep(delay)
                rfile = self.__ofilehandle('dos_w', energy, read=True)
                data = rfile.readline()
            dos.append(float(data.split()[-1]))
        return array(dos)

    def read_trans(self, wfilename, dos=None):
        """Read in transmission and DOS from wfilename"""
        delay = self.delay
        data = open(wfilename, 'r').readlines()
        lastenergy = float(self.__energy_to_str(self.all_energies[-1]))*1.5
        read_dos = dos is None
        # Check if the last line has been written to the file
        while float(data[-1].split()[0]) < lastenergy:
            self.verboseprint(wfilename, data[-1].split()[0], '<', lastenergy)
            sleep(delay)
            data = open(wfilename, 'r').readlines()
        energies = zeros(len(data))
        transmission = zeros(len(data))
        if read_dos:
            dos = zeros(len(data))
        for i, line in enumerate(data):
            info = line.split()
            energies[i] = float(info[0])
            transmission[i] = float(info[1])
            if read_dos:
                dos[i] = float(info[2])
            elif self.debug:
                assert abs(energies[i] - self.all_energies[-len(data)+i]) < 1e-9
        return self.get_efficiency(energies[-len(dos):],
                                   transmission[-len(dos):],
                                   dos)

    def get_efficiency(self, energies, transmission, dos):
        """Calculate EQE from DOS and Transmission
        energies	energy range in eV
        transmission	transmission probability [0,1]
	dos		density of states in [1/eV]"""
        if self.debug:
            assert len(dos) == len(energies) == len(transmission)
        if len(transmission) == 1:
            return transmission[0]
        normalization = trapz(y=dos, x=energies)
        efficiency = trapz(y=dos*transmission, x=energies) / normalization
        self.verboseprint(self.__energy_to_str(-energies[0]), normalization, efficiency)
        return efficiency

    def calculate_intensity(self, h_scatt, s_scatt, leads,
                            couplingindices, wfile, energies=None, dos=None,
                            vb_max=None):
        """Calculat the EQE for a given omega
        energies  Initial state energies at which to calculate the transmission
	dos	  Density of States (DOS) in 1/eV at the given energies
	vb_max	  Valence band maximum energy in eV relative to the Fermi level.
	  We do not calculate the transmission for energies above vb_max"""
        # The energies and dos arrays must have the same length
        if self.debug:
            assert dos is None or len(energies) == len(dos)
        left = self.left
        right = self.right
        leftprincipallayer = leads[left].principallayer
        rightprincipallayer = leads[right].principallayer
        hmat = ScatteringHamiltonianMatrix(leftprincipallayer=leftprincipallayer,
                                           rightprincipallayer=rightprincipallayer,
                                           hamiltonianmatrix=h_scatt,
                                           overlapmatrix=s_scatt,
                                           leftlead=leads[left],
                                           rightlead=leads[right],
                                           scattering=couplingindices)
        trans = TransmissionTool(hmat,
                                 leads[left],
                                 leads[right],
                                 energies,
                                 leftprincipallayer)
        trans.InitMultiLeadScatteringGreenFunction(leads, left, right)
        width = self.eta
        trans.GetLeftLeadGreenFunction().SetInfinitesimal(width)
        trans.GetRightLeadGreenFunction().SetInfinitesimal(width)
        trans.GetScatteringGreenFunction().SetInfinitesimal(width)
        #trans.GetScatteringGreenFunction().WideBandLimitOn()
        trans.UpdateTransmission()
        trans.GetScatteringGreenFunction().ReferenceGreenFunctionOn()
        trans.GetScatteringGreenFunction().ReferenceSelfEnergiesOn()
        transmission = []
        for i, energy in enumerate(energies):
            # Reset probability of no transmission to 1
            prob_notrans = 1
            # Do not calculate for energies above vb_max
            if not(self.efficient and energy > vb_max + self.domega):
                trans.GetScatteringGreenFunction().SetEnergy(energy)
                #transmission.append(abs(trans.GetTransmissionCoefficient()))
                trans_munu = trans.GetTransmissionFunction()
                tevals = sort(abs(eigvals(trans_munu)))
                for teval in tevals:
                    prob_notrans *= 1 - teval
            # Probability of transmission
            prob_trans = 1 - prob_notrans
            transmission.append(prob_trans)
            print(self.__energy_to_str(energy),
                  transmission[i],
                  dos[i],
                  file=wfile)
        return self.get_efficiency(energies, transmission, dos)
    	#trans.UpdateDOS()
	#filename = name+'_'+str(left)+'_'+str(right)+'_input.dat'
    	#trans.UpdateToTextFile(filename)

    def get_dos(self, energies, omega, occ, unocc, gamma=0.025, sigma=0.025):
        """ Return the combined density of states"""
        dosd = zeros(energies.shape)
        dosa = zeros(energies.shape)
        prefactor = gamma/(2*sigma**2)
        #for e_1 in occ:
        for e_1 in occ:
            dosd[:] += exp(-(e_1-energies[:])**2/(2*sigma**2))
        for e_2 in unocc:
            dosa[:] += exp(-(e_2-energies[:]-omega)**2/(2*sigma**2))
        return prefactor*dosd[:]*dosa[:]

    def calculate_kpt(self, kpt=0, spin=0, read_dos=False):
        """Calculate EQE for a particular k-point and spin channel
        kpt	index of k-pointt in [0, # of kpoints)
        spin	index of spin in [0, # of spins)"""

        h_scatt, s_scatt, h_lead, s_lead, a_coupling = self.matrices.load_matrices(kpt, spin)
        #self.verboseprint('Hamiltonian Lead Matrix', h_lead.shape, h_lead)
        #self.verboseprint('Hamiltonian Scattering Matrix', h_scatt.shape, h_scatt)
        #self.verboseprint('Coupling Matrix', a_coupling.shape, a_coupling)
        self.__cd_dosdir()
        self.calculate_dos(h_scatt, s_scatt, h_lead, s_lead, self.all_energies)
        dos_occ = self.read_dos(self.all_energies)
        chdir('..')
        ofile = self.__ofilehandle('dos')
        for index, energy in enumerate(self.all_energies):
            print(self.__energy_to_str(energy),
                  dos_occ[index],
                  file=ofile)
        if self.calc_unocc_dos:
            # Calculate Unoccupied DOS
            self.__cd_dosdir(unoccupied=True)
            unocc_energies = self.get_unocc_energies()
            self.calculate_dos(h_scatt, s_scatt, h_lead, s_lead, unocc_energies)
            dos_unocc = self.read_dos(unocc_energies)
            chdir('..')
            for index, energy in enumerate(unocc_energies):
                print(self.__energy_to_str(energy),
                      dos_unocc[index],
                      file=ofile)
        ofile.close()
        #ofile = self.__ofilehandle('trans')
        self.__cd_qdir()
        for omega in self.frequencies:
            wfilename = self.__wfilename(omega)
            if exists(wfilename):
                self.verboseprint(wfilename, 'written')
                continue
            wfile = self.__ofilehandle(label='w', energy=omega)
            #energy_index = energy_index+len(self.all_energies)-len(self.frequencies)
            a_coupling_omega = a_coupling*sqrt(Ha/(omega+self.scissor))
            h_total, s_total = self.matrices.total_matrix(h_scatt,
                                                          s_scatt,
                                                          a_coupling_omega,
                                                          omega)
            leads, couplingindices = self.setup_leads(h_lead,
                                                      s_lead,
                                                      h_total.shape[0],
                                                      omega)
            #energies = arange(-omega, 0, domega)
            energy_index = abs(self.all_energies+omega).argmin()
            energies = self.all_energies[energy_index:]
            #dos = get_dos(energies, omega, occ, unocc)
            if read_dos:
                dos = None
            else:
                dos = dos_occ[energy_index:]
            # If performing an efficient calculation,
            # pass the appropriate valence band maximum
            # energy in eV relative to the Fermi level
            # So that the transmission is not calculated
            # for initial energies above the valence band maximum
            if self.efficient:
                vb_max = self.matrices.get_vb_max(kpt=kpt, spin=spin)
            else:
                vb_max = None
            self.calculate_intensity(h_total,
                                     s_total,
                                     leads,
                                     couplingindices,
                                     wfile,
                                     energies=energies,
                                     dos=dos,
                                     vb_max=vb_max)
            #print(self.__energy_to_str(omega+self.scissor), intensity, file=ofile)
        #ofile.close()
        chdir('../')

    def __cd_dosdir(self, unoccupied=False):
        """Change to DOS directory
        unoccupied	Change to Unoccupied DOS directory (False)"""
        dirname = 'dos_'
        if unoccupied:
            dirname += 'unocc'
        else:
            dirname += 'occ'
        if not exists(dirname):
            mkdir(dirname)
        chdir(dirname)

    def __cd_kptdir(self, kpt):
        """Change to the k-point directory
        kpt	k-point index"""
        nkpts = self.matrices.nkpts
        if self.debug:
            assert kpt in range(nkpts)
        if nkpts != 1:
            kptdir = 'kpt'+str(kpt)
            if not exists(kptdir):
                mkdir(kptdir)
            chdir(kptdir)

    def __ld_kptdir(self):
        """Leave the k-point directory"""
        nkpts = self.matrices.nkpts
        if nkpts != 1:
            chdir('../')

    def __cd_qdir(self):
        """Change to the polarization direction directory"""
        qdirlabel = self.dirs[self.matrices.qdir]
        if not exists(qdirlabel):
            mkdir(qdirlabel)
        chdir(qdirlabel)

    def __cd_spindir(self, spin):
        """Change to different spin directories
        for spin polarized calculations
        spin	spin channel, either 0 or 1"""
        nspins = self.matrices.nspins
        if self.debug:
            assert spin in [0, 1]
            assert spin in range(nspins)
        if nspins != 1:
            spindir = 's'+str(spin)
            if not exists(spindir):
                mkdir(spindir)
            chdir(spindir)

    def __ld_spindir(self):
        """Leave the spin directory"""
        nspins = self.matrices.nspins
        if nspins != 1:
            chdir('../')

    def __ofilename(self, label=None, energy=None):
        """Return output file name with given label
        label 	one of 'dos', 'eqe', 'tdos', or 'w'
        energy	if label is w, energy is energy in eV"""
        if self.debug:
            assert label in ['dos', 'eqe', 'w', 'dos_w', 'tdos']
        if label.endswith('w'):
            label += self.__energy_to_str(energy)
        if label == 'eqe':
            label += self.dirs[self.matrices.qdir]
        ofilename = self.outfilename+'_'
        ofilename += label+self.postpend
        return ofilename

    def __ofilehandle(self, label=None, energy=None, read=False):
        """Return output file handle with given label
        label	one of 'dos', 'tdos', or, 'eqe'"""
        ofilename = self.__ofilename(label, energy)
        if not read:
            ofile = open(ofilename, 'w', buffering=1)
        else:
            ofile = open(ofilename, 'r')
        return ofile

    def __wfilename(self, omega):
        """Return omega file name
        omega 	frequency for filename"""
        if self.debug:
            assert omega in self.frequencies
        wfilename = self.outfilename+'_w'
        wfilename += self.__energy_to_str(omega)+self.postpend
        return wfilename

    def get_outfilenames(self, getepsimax=False):
        """Get a list of valid output filenames"""
        filenames = listdir('.')
        outfilenames = []
        postpend = self.postpend
        prepend = self.outfilename.split(postpend)[0]+'_w'
        lenmax = 0
        namemax = None
        for filename in filenames:
            if filename.startswith(prepend) and filename.endswith(postpend):
                outfilenames.append(filename)
                mlen = len(open(filename, 'r').readlines())
                lenmax = max(lenmax, mlen)
                if lenmax == mlen:
                    namemax = filename
                    #self.verboseprint(lenmax, namemax)
        if not getepsimax:
            return outfilenames
        epsimax = []
        data = open(namemax, 'r').readlines()
        for line in data:
            epsimax.append(float(line.split()[0]))
        return outfilenames, epsimax

    def get_omega(self, filename):
        """Extract energy from file name"""
        postpend = self.postpend
        if self.debug:
            assert filename.endswith(postpend)
        omega = float(filename.split('_w')[-1].split(postpend)[0])
        return omega

    def __get_cmap(self):
        """Get Colour Dictionary"""
        cdict = {'red':  [(0.00, 0.00, 0.00),
                          (0.025, 0.0, 0.0),
                          (0.05, 0.25, 0.25),
                          (0.125, 1.00, 1.00),
                          (0.250, 1.00, 1.00),
                          (1.00, 1.00, 1.00),],
                 'green':[(0.00, 0.00, 0.00),
                          (0.125, 0.00, 0.00),
                          #(0.80, 1.00, 1.00),
                          (0.250, 0.00, 0.00),
                          (0.6, 1.00, 1.00),
                          (1.00, 1.00, 1.00),],
                 'blue': [(0.00, 0.00, 0.00),
                          (0.025, 0.50, 0.50),
                          (0.05, 0.25, 0.25),
                          (0.125, 1.00, 1.00),
                          (0.250, 0.00, 0.00),
                          (0.6, 0.00, 0.00),
                          (1.00, 1.00, 1.00),],}

        cmap = LinearSegmentedColormap('STM_Colormap', cdict)
        return cmap



    def calculate(self, read_dos=False):
        """Calculate EQE for all k-points and spin channels"""
        if self.calculated:
            return
        nspins = self.matrices.nspins
        nkpts = self.matrices.nkpts
        for spin in range(nspins):
            self.__cd_spindir(spin)
            for kpt in range(nkpts):
                self.__cd_kptdir(kpt)
                self.calculate_kpt(kpt, spin, read_dos=read_dos)
                self.__ld_kptdir()
            self.sum_all_kpts(read_dos, spin)
            self.__ld_spindir()
        self.calculated = True

    def get_efficiencies(self, read_dos=False, spin=0):
        """Get external quantum efficiencies for a given spin channel
        spin	spin channel, either 0 or 1
        read_dos	Read DOS from output files"""
        if self.debug and spin:
            # Check that we have a spin channel and spin is either 0 or 1
            assert spin == 1 and self.matrices.nspins == 2
        if read_dos or not self.calculated:
            self.calculate(read_dos)
        return self.efficiencies[spin]

    def get_unocc_energies(self):
        """Returns array of unoccupied energies in eV"""
        return self.all_energies - self.all_energies[0] + self.dvb

    def get_frequencies(self):
        """Get frequencies in eV at which efficiencies are calculated"""
        return self.frequencies

    def sum_all_kpts(self, read_dos=False, spin=0):
        """Read in calculated EQE and sum over k-points"""
        nkpts = self.matrices.nkpts
        weights = self.matrices.wts
        ofile = self.__ofilehandle(label='eqe')
        efficiencies = self.efficiencies[spin]
        for frequency_index, omega in enumerate(self.frequencies):
            efficiencies[frequency_index] = 0
            for kpt in range(nkpts):
                self.__cd_kptdir(kpt)
                weight = weights[kpt]
                if read_dos:
                    dos = None
                else:
                    self.__cd_dosdir()
                    energy_index = abs(self.all_energies+omega).argmin()
                    dos = self.read_dos(self.all_energies)[energy_index:]
                    chdir('../')
                self.__cd_qdir()
                wfilename = self.__wfilename(omega)
                efficiencies[frequency_index] += weight*self.read_trans(wfilename, dos)
                chdir('../')
                self.__ld_kptdir()
            print(omega, efficiencies[frequency_index], file=ofile)
        ofile.close()
        if dos is None:
            return
        total_dos = self.all_energies*0
        unocc_energies = self.get_unocc_energies()
        total_unocc_dos = unocc_energies*0
        ofile = self.__ofilehandle(label='tdos')
        for kpt in range(nkpts):
            self.__cd_kptdir(kpt)
            weight = weights[kpt]
            self.__cd_dosdir()
            total_dos += weight*self.read_dos(self.all_energies)
            chdir('../')
            if self.calc_unocc_dos:
                self.__cd_dosdir(unoccupied=True)
                total_unocc_dos += weight*self.read_dos(unocc_energies)
                chdir('../')
            self.__ld_kptdir()
        for i, energy in enumerate(self.all_energies):
            print(energy, total_dos[i], file=ofile)
        if self.calc_unocc_dos or not self.efficient:
            for i, energy in enumerate(unocc_energies):
                print(energy, total_unocc_dos[i], file=ofile)
        ofile.close()
        self.total_dos = total_dos

    def add_zeros(self):
        """Adds zeros to transmission files for plotting"""
        nspins = self.matrices.nspins
        nkpts = self.matrices.nkpts
        for spin in range(nspins):
            self.__cd_spindir(spin)
            for kpt in range(nkpts):
                self.__cd_kptdir(kpt)
                self.__cd_qdir()
                outfilenames, epsimax = self.get_outfilenames(getepsimax=True)
                for filename in outfilenames:
                    #self.verboseprint(filename)
                    data = open(filename, 'r').readlines()
                    ofile = open(filename, 'w')
                    epsi_1 = float(data[0].split()[0])
                    #self.verboseprint(epsi_1, epsimax[0])
                    for epsimax_n in epsimax:
                        if epsi_1 > epsimax_n:
                            print(self.__energy_to_str(epsimax_n),
                                  0.0,
                                  0.0,
                                  file=ofile)
                    for line in data:
                        print(line.strip(), file=ofile)
                    ofile.close()
                chdir('../')
                self.__ld_kptdir()
            self.__ld_spindir()

    def plot_transmission(self, transmax=1.0, usemax=False):
        """Plot the transmission for each k-point
        transmax	maximum transission probability to plot (1)
        usemax		uise the maximum of the calculated transmission (False)"""
        nspins = self.matrices.nspins
        self.add_zeros()
        weights = self.matrices.wts
        for spin in range(nspins):
            self.__cd_spindir(spin)
            tot_frequency_ew = None
            tot_energy_ew = None
            tot_trans_ew = None
            for kpt in range(self.matrices.nkpts):
                self.__cd_kptdir(kpt)
                self.__cd_qdir()
                outfilenames = self.get_outfilenames()
                frequency_ew = []
                energy_ew = []
                trans_ew = []
                for filename in outfilenames:
                    data = open(filename, 'r').readlines()
                    omega = self.get_omega(filename)
                    energy_e = []
                    trans_e = []
                    for line in data:
                        energy_e.append(float(line.split()[0]))
                        trans_e.append(float(line.split()[1]))
                    energy_ew.append(energy_e)
                    trans_ew.append(trans_e)
                    frequency_ew.append(omega*ones(len(trans_e)))
                if tot_energy_ew is None:
                    tot_energy_ew = array(energy_ew)
                    tot_frequency_ew = array(frequency_ew)
                    if self.debug:
                        assert tot_energy_ew.shape == tot_frequency_ew.shape
                    tot_trans_ew = zeros(tot_energy_ew.shape)
                tot_trans_ew += weights[kpt]*array(trans_ew)
                chdir('../')
                self.__ld_kptdir()
            cmap = self.__get_cmap()
            triang = Triangulation(tot_frequency_ew.flatten(),
                                   tot_energy_ew.flatten())
            transmin = 0.0
            if usemax:
                transmax = tot_trans_ew.max()
            if self.debug:
                assert 0 < transmax <= 1
            dtrans = transmax/200.
            self.verboseprint("Trans Max", transmax)
            p.subplot(111)
            transrange = arange(transmin, transmax, dtrans)
            p.tricontourf(triang, tot_trans_ew.flatten(),
                          transrange, cmap=cmap)
            p.ylim(self.all_energies[0], self.all_energies[-1])
            p.xlim(self.frequencies[0], self.frequencies[-1])
            p.tick_params(axis='both',
                          bottom=True,
                          top=False,
                          left=False,
                          right=False)
            p.colorbar()
            graphname = self.outfilename
            graphname += '_'+self.dirs[self.matrices.qdir]+'.eps'
            p.savefig(graphname)
            self.__ld_spindir()

def main():
    """Command Line Execuatble"""
    # Read Arguments
    args = read_arguments()
    # Initialize EQE Matrix Elements
    eqe = ExternalQuantumEfficiency(filename=args.filename,
                                    verbose=args.verbose,
                                    tightbinding=args.tightbinding,
                                    size=args.size,
                                    energygap=args.energygap,
                                    coupling=args.nncoupling,
                                    nabla=args.nabla,
                                    volume=args.volume,
                                    wmin=args.omegamin,
                                    wmax=args.omegamax,
                                    domega=args.domega,
                                    dvb=args.dvb,
                                    qdir=args.qdir,
                                    pdir=args.pdir,
                                    eta=args.eta,
                                    eta_dos=args.eta_dos,
                                    scissor=args.scissor,
                                    efficient=args.efficient,
                                    debug=args.debug,
                                    calc_unocc_dos=args.calc_unocc_dos)
    eqe.calculate(read_dos=args.read_dos)
    if args.addzeros:
        eqe.add_zeros()
    if args.plot:
        eqe.plot_transmission(args.transmax, args.usemax)
