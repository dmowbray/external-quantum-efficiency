#!/usr/bin/python

"""Classes for the creation of the Hamiltonian and Overlap Matrices"""

from numpy import zeros, triu, tril, sqrt, pi, array, dtype, identity, empty

from ase.units import Ha, Bohr

from gpaw import GPAW
from gpaw import __version__ as gpaw_version
from gpaw.lcao.tools import get_lcao_hamiltonian
from gpaw.lcao.tools import remove_pbc as lcao_remove_pbc

class GenerateMatrices(object):
    """Interface to Generate Matrix Elements for the Hamiltonian and Overlaps"""
    def __init__(self, fermi_level, volume, qdir, pdir, nkpts, nspins, vb_max=None):
        """Generate Hamiltonian, Overlap, and Coupling Matrices for
        External Quantum Efficiency Calculation
        fermi_level	Fermi energy in eV
        volume	volume in a0^3
        qdir	polarization direction
        pdir	propagation direction
        nkpts	# of k-points
        nspins	# of spin channels
        vb_max	[nspins,nkpts] array of valence band maximum energies
		for each spin channel and k-point in eV
		relative to fermi_level"""
        self.fermi_level = fermi_level
        self.vb_max = vb_max
        self.volume = volume
        assert qdir in [0, 1, 2]
        self.qdir = qdir
        assert pdir in [0, 1, 2]
        self.pdir = pdir
        self.nkpts = nkpts
        assert nspins in [1, 2]
        self.nspins = nspins
        if vb_max is None:
            self.vb_max = zeros([nspins, nkpts])
        #print(vb_max.shape, (nspins, nkpts))
        assert vb_max.shape == (nspins, nkpts)
        # -i e/(2 m_e * c) * sqrt(8*pi*hbar/(4*pi*eps0))
        self.factor = -1j/137.036*sqrt(8*pi)*Ha
        # Check that all k-points are zero in the propogation direction pdir
        self.h_sknumu, self.s_knumu = self.generate_matrix()
        self.grad_phi_kqvnumu = self.generate_interaction()

    def __repr__(self):
        """String Representation of the GenerateMatrix Class"""
        dirs = ['x', 'y', 'z']
        strrep = "εF=%.2f eV," % self.fermi_level
        strrep += " V=%.2f a₀³," % self.volume
        strrep += " p="+dirs[self.pdir]
        strrep += ", q="+dirs[self.qdir]
        strrep += ", Nk=%s" % self.nkpts
        strrep += ", Ns=%s" % self.nspins
        return strrep

    def generate_matrix(self):
        """Interface Method for generation of
        hamiltonian and overlap matrices"""
        raise NotImplementedError

    def generate_interaction(self):
        """Interface Method for generation of
        coefficients"""
        raise NotImplementedError

    def remove_pbc(self, h_numu, s_numu=None):
        """Interface Method for removal of
        periodic boundary conditions from a
        matrix"""
        raise NotImplementedError

    def enlarge(self, h_numu, v_numu, interaction=False):
        """Enlarge Matrix"""
        prin = h_numu.shape[0]
        total = 3 * prin
        h_scatt = zeros([total, total], dtype=dtype(complex))
        # Initialize Scattering Matrix
        # Onsite Elements
        if not interaction:
            h_scatt[:prin, :prin] = h_numu[:, :]
            h_scatt[2*prin:, 2*prin:] = h_numu[:, :]
        h_scatt[prin:2*prin, prin:2*prin] = h_numu[:, :]
        # Couplings
        v_u = triu(v_numu.conj())
        v_l = tril(v_numu.conj())
        h_scatt[:prin, prin:prin*2] = v_l[:, :]
        h_scatt[prin:prin*2, :prin] = v_u[:, :]
        h_scatt[prin:2*prin, 2*prin:] = v_l[:, :]
        h_scatt[2*prin:, prin:2*prin] = v_u[:, :]
        return h_scatt

    def load_matrices(self, kpt=0, spin=0):
        """Returns the scattering and lead Hamiltonian and Overlap matrices and Coupliongs
        kpt	index of k-pointt in [0, # of kpoints)
        spin	index of spin in [0, # of spins)"""
        # Load Hamiltonian and Overlap Matrix for given system
        h_numu, s_numu, v_numu, sv_numu = self.get_matrices_overlaps(kpt, spin)
        prin = h_numu.shape[0]
        h_scatt = self.enlarge(h_numu, v_numu)
        s_scatt = self.enlarge(s_numu, sv_numu)

        h_lead = h_scatt[:2*prin, :2*prin].copy()
        s_lead = s_scatt[:2*prin, :2*prin].copy()
        h_lead[prin:, prin:] = h_scatt[:prin, :prin]
        s_lead[prin:, prin:] = s_scatt[:prin, :prin]
        a_numu, av_numu = self.load_interaction(kpt)
        #print("Coupling Matrix", a_numu)
        a_coupling = self.enlarge(a_numu, av_numu, interaction=True)
        #print("Enlarged Coupling Matrix", a_coupling)
        return h_scatt, s_scatt, h_lead, s_lead, a_coupling


    def total_matrix(self, h_scatt, s_scatt, a_coupling, omega=None):
        """Generate total matrix from scattering and coupling matrices"""
        prin = h_scatt.shape[0]
        pint = a_coupling.shape[0]
        assert prin == pint
        if omega is None:
            return h_scatt, s_scatt
        total = 2 * prin

        h_total = zeros([total, total], dtype=dtype(complex))
        h_total[:prin, :prin] = h_scatt[:, :]

        h_total[:pint, prin:prin+pint] = a_coupling
        h_total[prin:prin+pint, :pint] = a_coupling.T.conj()

        h_total[prin:2*prin, prin:2*prin] = h_scatt[:, :] - omega * s_scatt

        s_total = zeros([total, total], dtype=type(s_scatt[0, 0]))
        s_total[:prin, :prin] = s_scatt[:, :]
        s_total[prin:2*prin, prin:2*prin] = s_scatt[:, :]

        return h_total, s_total

    def generate_matrices(self, kpt, spin):
        """Place-holder method for generating matrices
        Returns the hamiltonian and overlap matrices and
        Fermi level"""
        assert spin < self.nspins
        assert kpt < self.nkpts
        return self.h_sknumu[spin, kpt], self.s_knumu[kpt], self.fermi_level

    def get_vb_max(self, kpt, spin=0):
        """Return top of the valence band in eV relative to the Fermi Level
        kpt	k-point index in [0, nkpts]
        spin	spin channel in [0,nspins]"""
        assert kpt in range(self.nkpts) and spin in range(self.nspins)
        return self.vb_max[spin, kpt]

    def get_matrices_overlaps(self, kpt=0, spin=0):
        """Generates the Hamiltonian and Overlap matrices and their Coupliongs
        kpt	index of k-pointt in [0, # of kpoints)
        spin	index of spin in [0, # of spins)"""
        #    if gpwfilename == 'Toy.gpw':
        #        h_numu, s_numu =  generate_toy_matrix(tau=tau)
        #        fermi_level = 0
        #    else:
        #        calc = GPAW(gpwfilename)
        #        fermi_level = calc.get_fermi_level()
        #        atoms = calc.get_atoms()
        #        h_sknumu, s_knumu = get_lcao_hamiltonian(calc)
        #        print(h_sknumu.shape)
        #        del calc
        #        h_numu = h_sknumu[0, kpt]
        #        s_numu = s_knumu[kpt]
        h_numu, s_numu, fermi_level = self.generate_matrices(kpt, spin)
        h_numu -= fermi_level * s_numu
        v_numu = h_numu.copy()
        sv_numu = s_numu.copy()
        h_numu, s_numu = self.remove_pbc(h_numu, s_numu)
        v_numu -= h_numu
        sv_numu -= s_numu
        return h_numu, s_numu, v_numu, sv_numu

    def load_interaction(self, kpt=0):
        """Load excitation interaction
         -i/c sqrt(8 pi) [a.u.]
         -i e/(2 m_e * c) * sqrt(8*pi*hbar/(4*pi*eps0))
        kpt	k-point index (0)"""
        qdir = self.qdir
        factor = self.factor / sqrt(self.volume)
        a_numu = self.grad_phi_kqvnumu[kpt, qdir] * factor
        av_numu = a_numu.copy()
        a_numu = self.remove_pbc(a_numu)[0]
        av_numu -= a_numu
        return a_numu, av_numu


class TBMatrices(GenerateMatrices):
    """Tight-Binding Matrices Implementation of GenerateMatrices class"""

    def __init__(self, size, energygap, coupling, nabla,
                 fermi_level=0, volume=8*pi, qdir=2, pdir=2):
        """Generate a Tight-Binding Hamiltonian
        size 		number of onsite elements
        energygap 	electronic band gap in eV
        coupling	element coupling, 1/4 the band widths, in eV
        nabla		the excitation overlap <nabla> in a.u.
        fermi_level	Fermi level (0 eV)
        volume		unit cell volume in a0^3 (8 pi a0^3)
        qdir		polarization direction (2)
        pdir		propagation direction (2)"""
        self.size = size
        self.energygap = energygap
        nkpts = 1	# One k-point
        nspins = 1 	# One spin channel
        # Valence Band Maximum is half gap relative to Fermi level
        vb_max = zeros([nspins, nkpts])
        vb_max += -energygap/2 - fermi_level
        self.coupling = coupling
        self.nabla = nabla
        self.kpts = [[0., 0., 0.]]	# Gamma-point
        self.wts = [1.]	# One k-point
        super(TBMatrices, self).__init__(fermi_level,
                                         volume,
                                         qdir,
                                         pdir,
                                         nkpts,
                                         nspins,
                                         vb_max,)

    def __repr__(self):
        """String Representation of the TB Matrix Class"""
        strrep = super(TBMatrices, self).__repr__()
        strrep += ", M=%s" % self.size
        strrep += ", Egap=%.2f eV" % self.energygap
        strrep += ", γ=%.2f eV" % self.coupling
        strrep += ", ⟨∇ ⟩=%.2f a.u." % self.nabla
        return strrep

    def generate_matrix(self):
        """Generate Tight-Binding Hamiltonian and Overlap Matrix"""
        size = self.size
        energygap = self.energygap
        coupling = self.coupling
        epsilon = 0.5*energygap + 2*coupling
        s_numu = identity(size, dtype=dtype(complex))
        h_numu = zeros([size, size], dtype=dtype(complex))
        for i in range(int(size/2)):
            h_numu[2*i, 2*i] = -epsilon
            h_numu[2*i-1, 2*i-1] = epsilon
        h_numu[-size+2:, :size-2] += coupling*identity(size-2)
        h_numu[:size-2, -size+2:] += coupling.conjugate()*identity(size-2)
        h_numu[-2:, :2] += coupling.conjugate()*identity(2)
        h_numu[:2, -2:] += coupling*identity(2)
        # Enlarge matricies to incorporate spin and k-point dependence
        self.h_sknumu = array([[h_numu]])
        self.s_knumu = array([s_numu])
        return self.h_sknumu, self.s_knumu

    def generate_interaction(self):
        """Generate the <phi|nabla|phi> matrix elements"""
        size = self.size
        nabla = self.nabla
        grad_numu = zeros([size, size], dtype=dtype(complex))
        grad_numu[:size-1, -size+1:] += nabla*identity(size-1)
        grad_numu[-size+1:, :size-1] += nabla.conjugate()*identity(size-1)
        grad_phi_kqvnumu = array([[grad_numu, grad_numu, grad_numu]])
        return grad_phi_kqvnumu

    def remove_pbc(self, h_numu, s_numu=None):
        """Remove Periodic boundary conditions in the given direction d
        h_numu  hamiltonian matrix elements
        s_numu	overlap matrix elements"""

        h_numu[-2:, :2] *= 0
        h_numu[:2, -2:] *= 0
        if s_numu is None:
            return h_numu, s_numu
        s_numu[-2:, :2] *= 0
        s_numu[:2, -2:] *= 0
        return h_numu, s_numu

class LCAOMatrices(GenerateMatrices):
    """Read DFT Matrix Elements from LCAO Calculation"""
    def __init__(self, filename, qdir=2, pdir=2):
        """Initialize LCAO matrices
        filename	GPAW LCAO DFT gpw file
        qdir		polarization direction (2)
        pdir		propagation direction (2)"""
        calc = GPAW(filename, txt=None)
        self.calc = calc
        nspins = calc.get_number_of_spins()
        self.kpts = calc.get_ibz_k_points()
        for kpt in self.kpts:
            assert kpt[pdir] == 0
        nkpts = len(self.kpts)
        self.wts = calc.get_k_point_weights()
        self.atoms = calc.get_atoms()
        volume = self.atoms.get_volume()/(Bohr**3)
        vb_max, fermi_level, energygap = self.__get_vb_max(calc)
        self.energygap = energygap
        super(LCAOMatrices, self).__init__(fermi_level,
                                           volume,
                                           qdir,
                                           pdir,
                                           nkpts,
                                           nspins,
                                           vb_max)
        del self.calc

    def __repr__(self):
        """String Representation of the LCAO Matrix Class"""
        strrep = super(LCAOMatrices, self).__repr__()
        strrep += ", "+str(self.atoms)
        return strrep

    def __get_vb_max(self, calc):
        """Return array of valence band maximums in eV
        relative to Fermi level"""
        # Valence Band index
        vb_i = int(calc.get_number_of_electrons()/2) - 1
        nspins = calc.get_number_of_spins()
        nkpts = len(self.kpts)
        vb_max = zeros([nspins, nkpts])
        fermi_level = calc.get_fermi_level()
        energygaps = zeros([nspins, nkpts])
        for spin in range(nspins):
            for kpt in range(nkpts):
                vbm_cbm = calc.get_eigenvalues(kpt=kpt, spin=spin)[vb_i:vb_i+2]
                energygaps[spin, kpt] = vbm_cbm[1] - vbm_cbm[0]
                vb_max[spin, kpt] = vbm_cbm[0] - fermi_level
        return vb_max, fermi_level, energygaps.min()

    def generate_matrix(self):
        """Load LCAO DFT Hamiltonian and Overlap Matrix
        Depends on LCAO tools package get_lcao_hamiltonian"""
        calc = self.calc
        return get_lcao_hamiltonian(calc)

    def generate_interaction(self):
        """Load LCAO DFT <nabla> matrix elements
        Code taken from LCAO-TDDFT-k-omega"""
        # Depends on LCAO-TDDFT-k-omega Package
        #return LCAOTDDFTq0(self.calc).grad_phi_kqvnumu
        ksl = self.calc.wfs.ksl
        if gpaw_version >= '1.5.0':
            gcomm = self.calc.wfs.gd.comm
            manytci = self.calc.wfs.manytci
            grad_phi_kqvnumu = manytci.O_qMM_T_qMM(gcomm,
                                                   ksl.Mstart,
                                                   ksl.Mstop,
                                                   ignore_upper=ksl.using_blacs,
                                                   derivative=True)[0]
        else:
            spos_ac = self.calc.get_atoms().get_scaled_positions() % 1.0
            nkpts = len(self.calc.wfs.kd.ibzk_qc)
            mydtype = self.calc.wfs.dtype
            grad_phi_kqvnumu = empty((nkpts, 3, ksl.mynao, ksl.nao), mydtype)
            dtdr_kqvnumu = empty((nkpts, 3, ksl.mynao, ksl.nao), mydtype)
            dprojdr_aqvnui = {}
            for natom in self.calc.wfs.basis_functions.my_atom_indices:
                i = self.calc.wfs.setups[natom].ni
                dprojdr_aqvnui[natom] = empty((nkpts, 3, ksl.nao, i), mydtype)
            self.calc.wfs.tci.calculate_derivative(spos_ac,
                                                   grad_phi_kqvnumu,
                                                   dtdr_kqvnumu,
                                                   dprojdr_aqvnui)

        # If using BLACS initialize lower triangle.
        if self.calc.wfs.ksl.using_blacs:
            for i in range(grad_phi_kqvnumu.shape[-1]):
                grad_phi_kqvnumu[:, :, i, i:] = -grad_phi_kqvnumu[:, :, i:, i]
        #print(type(grad_phi_kqvnumu))
        return array(grad_phi_kqvnumu)

    def remove_pbc(self, h_numu, s_numu=None):
        """Remove Periodic boundary conditions in the propagation direction pdir
        Depends on LCAO tools package remove_pbc
        h_numu  hamiltonian matrix elements
        s_munu	overlap matrix elements"""
        lcao_remove_pbc(self.atoms, h_numu, s_numu, d=self.pdir)
        return h_numu, s_numu
