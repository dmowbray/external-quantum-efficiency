# Copyright (c) 2008 CAMd

"""A container class for atoms.
The class `ListOfSlices' is derived from python's built-in ``list''."""

class ListOfSlices(list):
    """A container class for slices, which contains several
    convenience methods for manipulating lists of slices."""
    def __init__(self, slices):
        list.__init__(self, slices)
        self.Compactify()

    def append(self, slice):
        list.append(self, slice)
        self.Compactify()

    def extend(self, listofslices):
        list.extend(self, listofslices)
        self.Compactify()

    def WrapNegativeSlices(self, end):
        """Wraps Negative slices relative to ``end''"""
        for i in range(len(self)):
            start = self[i].start
            stop = self[i].stop
            if start < 0:
                start += end
                stop += end
            elif stop < 0:
                stop += end
            self[i] = slice(start, stop)

    def Copy(self):
        return ListOfSlices(self)

    def len(self, n=None):
        if n is None:
            return sum([self.len(n) for n in range(len(self))])
        return self[n].stop-self[n].start

    def SliceIndices(self, n=None):
        if n is None:
            return self.len()
        return sum([slist.len(m) for m in range(n)])

    def Difference(self, begin=0, end=None):
        """Returns the difference between the slice ``range''
        and the ListOfSlices"""
        if end is None:
            print("Undefined Range")
            return
        sorted = self.Copy()
        sorted.WrapNegativeSlices(end)
        sorted.sort()
        diff = ListOfSlices([])
        slicestart = begin
        for n in range(len(sorted)):
            start = sorted[n].start
            if start < begin:
                continue
            if start > slicestart:
                if start >= end:
                    if slicestart < end:
                        diff.append(slice(slicestart, end))
                    return diff
                diff.append(slice(slicestart, start))
            slicestart = sorted[n].stop
        if slicestart < end:
            diff.append(slice(slicestart, end))
        return diff

    def Compactify(self):
        self.compact = []
        compact = self.compact
        start = 0
        for n in range(len(self)):
            stop = start+self.len(n)
            compact.append(slice(start, stop))
            start = stop

    def GetCompactSlices(self):
        return self.compact

    def SetScatteringSlices(self, scatteringslices):
        self.scattering = ListOfSlices([])
        scattering = self.scattering
        compact = scatteringslices.GetCompactSlices()
        for i in range(len(self)):
            start = self[i].start
            stop = self[i].stop
            for j in range(len(scatteringslices)):
                scatstart = scatteringslices[j].start
                scatstop = scatteringslices[j].stop
                # shift of compact slices
                shift = scatstart - compact[j].start
                intstart = max(scatstart, start)
                intstop = min(scatstop, stop)
                if intstart < intstop:
                    # If there is a non-zero overlap
                    scattering.append(slice(intstart-shift, intstop-shift))
                    if scatstop >= stop:
                        # If self[i] doesn't extend beyond scatteringslices[j]
                        # Assume scatteringslices is ordered
                        break
        self.scattering = scattering

    def GetScatteringSlices(self):
        return self.scattering

    def GetShifted(self, shift):
        shiftedslices = ListOfSlices([])
        for n in range(len(self)):
            shiftedslices.append(slice(self[n].start+shift, self[n].stop+shift))
        return shiftedslices
