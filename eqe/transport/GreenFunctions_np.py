"""Green Functions of NEGF 4-terminal Code"""
#import Numeric
import numpy as np
#import LinearAlgebra
import numpy.linalg as LinearAlgebra
from numpy.linalg import eig as eigenvectors
import numpy.fft as FFT
#from Scientific.IO.NetCDF import NetCDFFile
from eqe.transport.Tools_np import FermiDistribution, LambdaFromSelfEnergy, Dagger
from eqe.transport.HamiltonianTools_np import RotateMatrix

# In addition the following modules are used:
# Scientific.IO.NetCDF

class GreenFunction:
    def __init__(self, hamiltonian=None, identity=None, energy=None, infinitesimal=1.e-4):
        if hamiltonian is not None:
            # To avoid that SetHamiltonianRepresentation
            # of call ScatteringGreenFunction is used
            GreenFunction.SetHamiltonianRepresentation(self, hamiltonian)
        if identity is not None:
            self.SetIdentityRepresentation(identity)
        if energy is not None:
            self.SetEnergy(energy)
        self.SetInfinitesimal(infinitesimal)

    def SetHamiltonianRepresentation(self, hamiltonian):
        self.hamiltonian = hamiltonian
        # This will force an update
        self.UnregisterRepresentation()

    def GetHamiltonianRepresentation(self):
        return self.hamiltonian

    def GetMatrixDimension(self):
        if not hasattr(self, 'shape'):
            self.shape = self.GetHamiltonianRepresentation().shape
        return self.shape

    def SetIdentityRepresentation(self, identity):
        self.identity = identity
        # This will force an update
        self.UnregisterRepresentation()

    def GetIdentityRepresentation(self):
        return self.identity

    def SetInfinitesimal(self, infinitesimal):
        self.inf = infinitesimal
        # This will force an update
        self.UnregisterRepresentation()

    def GetInfinitesimal(self):
        if not hasattr(self, 'inf'):
            self.inf = 1.0e-4
        return self.inf

    def SetEnergy(self, energy):
        """Lead Green Function Energy Set"""
        self.energy = energy
        # This will force an update
        self.UnregisterRepresentation()

    def GetEnergy(self):
        return self.energy

    def GetComplexEnergy(self):
        return complex(self.GetEnergy(), self.GetInfinitesimal())

    def ReferenceGreenFunction(self):
        # Default is *not* to reference Green function
        if not hasattr(self, "greenfunctionflag"):
            self.ReferenceGreenFunctionOff()
        return self.greenfunctionflag

    def ReferenceGreenFunctionOn(self):
        self.greenfunctionflag = 1

    def ReferenceGreenFunctionOff(self):
        self.greenfunctionflag = 0

    def SetRepresentation(self, representation):
        self.representation = representation

    def GetRepresentation(self):
        # If the representation is not set: update
        if not hasattr(self, "representation"):
            self.UpdateRepresentation()
        representation = self.representation

        # If not ReferenceGreenFunction: Delete the representation
        if not self.ReferenceGreenFunction():
            self.UnregisterRepresentation()
        return representation

    def UpdateRepresentation(self):
        hamiltonian = self.GetHamiltonianRepresentation()
        identity = self.GetIdentityRepresentation()
        # No copy of the matrix is performed: input -> output
        #greenfunction = ArrayTools.inv(
        #    self.GetComplexEnergy() * identity - hamiltonian, copymatrix=0)
        greenfunction = LinearAlgebra.inv(
            self.GetComplexEnergy() * identity - hamiltonian)
        self.SetRepresentation(greenfunction)

    def UnregisterRepresentation(self):
        # Try to unregister representation
        if hasattr(self, 'representation'):
            delattr(self, 'representation')

    def Set(self, **kwargs):
        for name, value in kwargs.items():
            method_name = 'Set' + name
            name = name.lower()
            if value is None:
                self.Unregister(name)
            elif hasattr(self, method_name):
                getattr(self, method_name)(value)
            else:
                setattr(self, name, value)

    def Unregister(self, *args):
        for name in args:
            method_name = 'Unregister' + name
            name = name.lower()
            if hasattr(self, method_name):
                getattr(self, method_name)()
            elif hasattr(self, name):
                delattr(self, name)
            else:
                print(name + ' already unregistered or invalid name')

class LeadGreenFunction(GreenFunction):
    def __init__(self,
                 hamiltonian=None,
                 interaction=None,
                 identity=None,
                 identityinteraction=None,
                 energy=None):
        GreenFunction.__init__(self, hamiltonian=hamiltonian,
                               identity=identity, energy=energy)
        if interaction is not None:
            self.SetInteractionRepresentation(interaction)
        if identityinteraction is not None:
            self.SetInteractionIdentityRepresentation(identityinteraction)

    def SetInteractionRepresentation(self, interaction):
        self.interaction = interaction
        # This will force an update
        self.UnregisterRepresentation()

    def GetInteractionRepresentation(self):
        return self.interaction

    def SetInteractionIdentityRepresentation(self, identityinteraction):
        self.identity_interaction = identityinteraction
        # This will force an update
        self.UnregisterRepresentation()

    def GetInteractionIdentityRepresentation(self):
        return self.identity_interaction

    def SetMaxNumberOfIterations(self, niterations):
        self.niterations = niterations

    def GetMaxNumberOfIterations(self):
        if not hasattr(self, 'niterations'):
            self.niterations = 100000
        return self.niterations

    def SetConvergenceFactor(self, convergence):
        self.convergence = convergence

    def GetConvergenceFactor(self):
        if not hasattr(self, 'convergence'):
            self.convergence = 1.0e-8
        return self.convergence

    def GetRetardedEquation(self):
        #multiply = ArrayTools.MatrixMultiplication
        multiply = np.dot

        # Calculating (EO-H) by iteration
        energy = self.GetComplexEnergy()

        # h00 = energy*I11-H11:
        h00 = energy*self.GetIdentityRepresentation()
        h00 = h00-self.GetHamiltonianRepresentation()
        # h11 = energy*I11-H11 = h00:
        h11 = h00.copy()
        # h10 = energy*Ii^dagger-Hi^dagger:
        h10 = energy*Dagger(self.GetInteractionIdentityRepresentation())
        h10 = h10-Dagger(self.GetInteractionRepresentation())
        # h01 = energy*Ii-Hi:
        h01 = energy*self.GetInteractionIdentityRepresentation()
        h01 = h01-self.GetInteractionRepresentation()
        delta = 10000
        n = 0
        while (delta > self.GetConvergenceFactor()) and \
                  (n < self.GetMaxNumberOfIterations()):
            # Calculating:
            # A = h_{11}^{-1}xh_{01}    B = h_{11}^{-1}xh_{10}
            # h_{11}xA = h_{01}         h_{11}xB = h_{10}

            A = LinearAlgebra.solve(h11, h01)
            B = LinearAlgebra.solve(h11, h10)

            # Then: h_{00} -> h_{00}-h_{01}xB
            #       h_{11} -> h_{11}-h_{10}xA-h_{01}xB
            #       h_{01} -> -h_{01}xA
            #       h_{10} -> -h_{10}xB
            # Calculate the "screened" interactions

            h01B = multiply(h01, B)
            h00 = h00 - h01B
            h11 = h11 - multiply(h10, A) - h01B
            h01 = -multiply(h01, A)
            h10 = -multiply(h10, B)

            # If element h01 is negligible: stop the iteration
            delta = max(np.absolute((h01).ravel()))
            n = n + 1
        return h00

#    def ReadRetardedEquationFromNetCDFFile(self, filename, lead):
#        # lead can be 'left' or 'right' depending on which lead should be read
#        file = NetCDFFile(filename, 'r')
#        dim = int(file.variables["Dimension"].getValue())
#        reteq = np.zeros([dim, dim], np.complex)
#        reteq.real = file.variables[lead].getValue()[:, :, 0]
#        reteq.imag = file.variables[lead].getValue()[:, :, 1]
#        file.close()
#        return reteq

    def UpdateRepresentation(self):
        leadgreenfunction = LinearAlgebra.inv(self.GetRetardedEquation())
        self.SetRepresentation(leadgreenfunction)

class ScatteringGreenFunction(GreenFunction):

    def __init__(self, scatteringhamiltonian=None, leftleadhamiltonian=None,
                 rightleadhamiltonian=None,
                 internalscatteringhamiltonian=None, energy=None):
        if scatteringhamiltonian is not None:
            self.SetScatteringHamiltonian(scatteringhamiltonian)
        if internalscatteringhamiltonian is not None:
            self.SetInternalScatteringHamiltonian(internalscatteringhamiltonian)
        if leftleadhamiltonian is not None:
            self.SetLeftLeadHamiltonian(leftleadhamiltonian)
        if rightleadhamiltonian is not None:
            self.SetRightLeadHamiltonian(rightleadhamiltonian)
        self.InitializeLeadGreenFunctions()

    def SetScatteringHamiltonian(self, hs):
        self.hsmatrix = hs

    def GetScatteringHamiltonian(self):
        return self.hsmatrix

    def SetInternalScatteringHamiltonian(self, hs):
        self.internalscatter = 1
        self.inthsmatrix = hs

    def GetInternalScatteringHamiltonian(self):
        return self.inthsmatrix

    def SetLeftLeadHamiltonian(self, hl):
        self.hlmatrix = hl

    def GetLeftLeadHamiltonian(self):
        assert hasattr(self, 'hlmatrix'), "Please specify the leftleadhamiltonian."
        return self.hlmatrix

    def SetRightLeadHamiltonian(self, hr):
        self.hrmatrix = hr

    def GetRightLeadHamiltonian(self):
        assert hasattr(self, 'hrmatrix'), "Please specify the rightleadhamiltonian."
        return self.hrmatrix

    def AlignFermiLevels(self, h00_l=None):
        """Aligns the fermi level of the scattering region
           to the fermi level of the lead, or to the argument e0.

           The aligning is done by matching a matrix element
           <w|H|w> (the first) in the principal layer
           in the scattering region to the corresponding matrix element
           in the lead"""

        #instances
        Hs = self.GetScatteringHamiltonian()
        Hl = self.GetLeftLeadHamiltonian()#assuming the leads are identical

        #matrices
        h00_s = Hs.GetH_l()[0, 0]
        s00_s = Hs.GetS_l()[0, 0]
        if h00_l is None:
            h00_l = Hl.GetH_l()[0, 0]
        S = Hs.GetOverlapMatrix()
        H = Hs.GetHamiltonianMatrix()
        ##print("diff:", h00_s - h00_l)
        #do the alignment
        Hs_aligned = H - ((h00_s - h00_l)/s00_s) * S

        #update the matrix of the Hs instance
        Hs.SetHamiltonianMatrix(Hs_aligned)

        # Do the same for Hartree and xc if they exist
        if hasattr(Hs, 'hartree'):
            Hs.SetHartreeMatrix(Hs.GetHartreeMatrix() - ((h00_s - h00_l)/s00_s) * S)
        if hasattr(Hs, 'xc'):
            Hs.SetXCMatrix(Hs.GetXCMatrix() - ((h00_s - h00_l)/s00_s) * S)
        if hasattr(self, 'inthsmatrix'):
            Hs_int = self.GetInternalScatteringHamiltonian()
            Hs_int.SetHamiltonianMatrix(Hs_aligned.copy())
            if hasattr(Hs_int, 'hartree'):
                Hs_int.SetHartreeMatrix(Hs_int.GetHartreeMatrix() -
                                        ((h00_s - h00_l)/s00_s) * S)
            if hasattr(Hs_int, 'xc'):
                Hs_int.SetXCMatrix(Hs_int.GetXCMatrix() - ((h00_s - h00_l)/s00_s) * S)
        ##print "Principal layer convergence: " + str(
        ##    self.GetPrincipalLayerConvergence()) + " eV\n"

    def GetPrincipalLayerConvergence(self, pl1=None, pl2=None):
        """The function returns a real number, which is the
           absolute value of the largest difference of the
           matrix elements of the principal layer in the lead
           and in the scattering region.
           These should be as small as possible
           (say below at least 0.2 eV)."""

        #instances
        Hs = self.GetScatteringHamiltonian()
        Hl = self.GetLeftLeadHamiltonian()#assuming the leads are identical

        #matrices
        if pl1 is None or pl2 is None:
            # no principal layers was specified ->
            # use the lead and S region p.l.
            pl1 = Hs.GetH_l() # leftmost principallayer in the S region
            pl2 = Hl.GetH_l() # principallayer in the lead

        err = max(np.absolute((pl2 - pl1).ravel()))
        return err

    def InitializeLeadGreenFunctions(self):
        # Initialize left lead GF
        H_ll = self.GetLeftLeadHamiltonian().GetH_l()
        O_ll = self.GetLeftLeadHamiltonian().GetS_l()
        H_intsc = self.GetLeftLeadHamiltonian().GetT_l()
        O_intsc = self.GetLeftLeadHamiltonian().GetInteractionS_l()
        self.SetLeftLeadGreenFunction(LeadGreenFunction(
            hamiltonian=H_ll, interaction=H_intsc, identity=O_ll,
            identityinteraction=O_intsc))

        # Initialize right lead GF
        H_rr = self.GetRightLeadHamiltonian().GetH_r()
        O_rr = self.GetRightLeadHamiltonian().GetS_r()
        H_intsc = self.GetRightLeadHamiltonian().GetT_r()
        O_intsc = self.GetRightLeadHamiltonian().GetInteractionS_r()
        self.SetRightLeadGreenFunction(LeadGreenFunction(
            hamiltonian=H_rr, interaction=H_intsc, identity=O_rr,
            identityinteraction=O_intsc))

        # Initialize parent Green function
        H_sc = self.GetScatteringHamiltonian().GetH_s()
        O_sc = self.GetScatteringHamiltonian().GetS_s()
        GreenFunction.__init__(self, hamiltonian=H_sc, identity=O_sc)

    def GetHamiltonianRepresentation(self):
        if hasattr(self, 'internalscatter'):
            return self.GetInternal_H_sc()
        return GreenFunction.GetHamiltonianRepresentation(self)

    def SetHamiltonianRepresentation(self, h0):
        if hasattr(self, 'internalscatter'):
            int_h = self.GetInternalScatteringHamiltonian()
            h = self.GetScatteringHamiltonian()

            # Setting internal Hamiltonian
            int_h.SetH_s(h0)

            # Setting internal part of large scattering Hamiltonian
            H_mat = h.GetHamiltonianMatrix()
            leftprinlength = int_h.GetLeftPrincipalLayerLength()
            rightprinlength = int_h.GetRightPrincipalLayerLength()
            length = H_mat.shape[0]
            H_mat[leftprinlength:length - rightprinlength,
                  leftprinlength:length-rightprinlength] = h0
            h.SetHamiltonianMatrix(H_mat)
            GreenFunction.SetHamiltonianRepresentation(self, h.GetH_s())
        else:
            GreenFunction.SetHamiltonianRepresentation(self, h0)

    def GetIdentityRepresentation(self):
        if hasattr(self, 'internalscatter'):
            return self.GetInternal_S_sc()
        return GreenFunction.GetIdentityRepresentation(self)

    def GetInternal_H_sc(self):
        return self.GetInternalScatteringHamiltonian().GetH_s()

    def GetInternal_S_sc(self):
        return self.GetInternalScatteringHamiltonian().GetS_s()

    def GetInternal_H_intl(self):
        return self.GetInternalScatteringHamiltonian().GetT_l()

    def GetInternal_S_intl(self):
        return self.GetInternalScatteringHamiltonian().GetInteractionS_l()

    def GetInternal_H_intr(self):
        return self.GetInternalScatteringHamiltonian().GetT_r()

    def GetInternal_S_intr(self):
        return self.GetInternalScatteringHamiltonian().GetInteractionS_r()

    def GetInternal_H_l(self):
        return self.GetInternalScatteringHamiltonian().GetH_l()

    def GetInternal_S_l(self):
        return self.GetInternalScatteringHamiltonian().GetS_l()

    def GetInternal_H_r(self):
        return self.GetInternalScatteringHamiltonian().GetH_r()

    def GetInternal_S_r(self):
        return self.GetInternalScatteringHamiltonian().GetS_r()

    def WideBandLimitOn(self):
        self.widebandlimit = 1

    def SetEnergy(self, energy):
        """Propagating changes to lead green functions"""
        GreenFunction.SetEnergy(self, energy)
        try:
            self.GetLeftLeadGreenFunction().SetEnergy(energy)
        except AttributeError:
            pass
        try:
            self.GetRightLeadGreenFunction().SetEnergy(energy)
        except AttributeError:
            pass
        # Force an update of self energies:
        self.UnregisterRightLeadSelfEnergy()
        self.UnregisterLeftLeadSelfEnergy()

    def SetLeftLeadGreenFunction(self, leftleadgreenfunction):
        """The lead green functions should not be referenced"""
        leftleadgreenfunction.ReferenceGreenFunctionOff()
        self.leftleadgf = leftleadgreenfunction

    def GetLeftLeadGreenFunction(self):
        return self.leftleadgf

    def SetRightLeadGreenFunction(self, rightleadgreenfunction):
        """The lead green functions should not be referenced"""
        rightleadgreenfunction.ReferenceGreenFunctionOff()
        self.rightleadgf = rightleadgreenfunction

    def GetRightLeadGreenFunction(self):
        return self.rightleadgf

    def GetLeftLeadInteractionRepresentation(self):
        return self.GetScatteringHamiltonian().GetT_l()

    def GetRightLeadInteractionRepresentation(self):
        return self.GetScatteringHamiltonian().GetT_r()

    def GetLeftLeadIdentityRepresentation(self):
        return self.GetScatteringHamiltonian().GetInteractionS_l()

    def GetRightLeadIdentityRepresentation(self):
        return self.GetScatteringHamiltonian().GetInteractionS_r()

    def ReferenceSelfEnergies(self):
        # Default: Self energies are not referenced:
        if not hasattr(self, "sigmatag"):
            self.ReferenceSelfEnergiesOff()
        return self.sigmatag

    def ReferenceSelfEnergiesOn(self):
        self.sigmatag = 1

    def ReferenceSelfEnergiesOff(self):
        self.sigmatag = 0

    def UpdateRightLeadSelfEnergy(self):
        # Calculating self energy due to lead i:
        # Sigma_{i} = tau_{right} x g_{right} x tau'_{right}

        # Finding [g_{right}]^{-1}
        retardedeq = self.GetRightLeadGreenFunction().GetRetardedEquation()
        if not hasattr(self, 'internalscatter'):
            # Calculating: tau' = E*I_I^{dagger}_I-H_I^{dagger},
            # where I_I is the overlap matrix and H_I is the hamiltonian
            tau_right = (self.GetComplexEnergy()
                         * Dagger(self.GetRightLeadIdentityRepresentation())
                         - Dagger(self.GetRightLeadInteractionRepresentation()))
            # Using that: A = g_{right} x tau'_{right} <=>
            #             h_{right} x A = tau'_{right}
            # h_{right} is the retarded equation of right lead
            A = LinearAlgebra.solve(retardedeq, tau_right)

            # Deleting obsolete variables
            del retardedeq


            # Then Sigma{right} =  tau_{right} x A,
            # tau_{right} = E*I_I - H_I
            tau_right = self.GetComplexEnergy() * self.GetRightLeadIdentityRepresentation() - \
                        self.GetRightLeadInteractionRepresentation()

            self.SetRightLeadSelfEnergy(np.dot(tau_right, A))
        else:
            hi = self.GetRightLeadGreenFunction().GetInteractionRepresentation()
            si = self.GetRightLeadGreenFunction().GetInteractionIdentityRepresentation()

            tau_right = self.GetComplexEnergy() * Dagger(si) - Dagger(hi)
            A = LinearAlgebra.solve(retardedeq, tau_right)

            # sigma below is the usual self-energy describing the coupling
            # of one extra principal layer to the semi-infinite lead.
            tau_right = self.GetComplexEnergy() * si - hi
            sigma = np.dot(tau_right, A)

            # The following retarded equation uses sigma as self-energy to
            # the lower right block of scattering Hamiltonian.
            # NOTE: Even if the internal region is the full scattering region,
            # there is still a difference coming from the fact that the lower
            # right block of scattering Hamiltonian in that case might be
            # different from the principal layer (the difference
            # will be larger the larger 'principal-layer convergence')
            zS = self.GetComplexEnergy() * self.GetInternal_S_r()
            retardedeq = zS - self.GetInternal_H_r()
            l = sigma.shape[0]
            retardedeq[-l:, -l:] = retardedeq[-l:, -l:] - sigma
            tau_right = self.GetComplexEnergy() * Dagger(self.GetInternal_S_intr()) - \
                        Dagger(self.GetInternal_H_intr())

            A = LinearAlgebra.solve(retardedeq, tau_right)
            del retardedeq
            tau_right = self.GetComplexEnergy() * self.GetInternal_S_intr() - \
                        self.GetInternal_H_intr()

            self.SetRightLeadSelfEnergy(np.dot(tau_right, A))

    def GetRightLeadSelfEnergy(self):
        # If the presentation is not set: update
        if not hasattr(self, "sigmaright"):
            self.UpdateRightLeadSelfEnergy()
        sigmaright = self.sigmaright
        # If not ReferenceSelfEnergies: Delete the self energy
        if not self.ReferenceSelfEnergies():
            self.UnregisterRightLeadSelfEnergy()
        return sigmaright

    def SetRightLeadSelfEnergy(self, selfenergy):
        self.sigmaright = selfenergy

    def UnregisterRightLeadSelfEnergy(self):
        # First unregister right lead self energy:
        if hasattr(self, "sigmaright") and (not hasattr(self, "widebandlimit")):
            delattr(self, "sigmaright")

    def UpdateLeftLeadSelfEnergy(self):
        # Calculating self energy due to lead i:
        # Sigma_{i} = tau_{left} x g_{left} x tau'_{left}

        # First finding [g_{left}]^{-1}
        retardedeq = self.GetLeftLeadGreenFunction().GetRetardedEquation()
        if not hasattr(self, "internalscatter"):
            # Calculating: tau' = E*I^{dagger}_I-H_I^{dagger},
            tau_left = self.GetComplexEnergy()*Dagger(self.GetLeftLeadIdentityRepresentation())
            tau_left -= Dagger(self.GetLeftLeadInteractionRepresentation())
            # Using that: A = g_{left} x tau'_{left} <=>
            #             h_{left} x A = tau'_{left}
            # h_{left} is the retarded equation of left lead
            A = LinearAlgebra.solve(retardedeq, tau_left)
            # Deleting obsolete variables
            del retardedeq
            # Then Sigma{left} =  tau x A
            # tau_left = energy*I_i-H_i
            tau_left = self.GetComplexEnergy()*self.GetLeftLeadIdentityRepresentation()
            tau_left -= self.GetLeftLeadInteractionRepresentation()
            #self.SetLeftLeadSelfEnergy(ArrayTools.MatrixMultiplication(tau_left, A))
            self.SetLeftLeadSelfEnergy(np.dot(tau_left, A))
        else:
            hi = self.GetLeftLeadGreenFunction().GetInteractionRepresentation()
            si = self.GetLeftLeadGreenFunction().GetInteractionIdentityRepresentation()
            # We assume that basis functions in the lead are orthonormal
            tau_left = self.GetComplexEnergy() * Dagger(si) - Dagger(hi)

            ##A = LinearAlgebra.solve(retardedeq, Dagger(hi))
            A = LinearAlgebra.solve(retardedeq, tau_left)
            tau_left = self.GetComplexEnergy() * si - hi
            sigma = np.dot(tau_left, A)

            # Here we get the energy from scattering Green function
            zS = self.GetComplexEnergy()*self.GetInternal_S_l()
            retardedeq = zS-self.GetInternal_H_l()
            l = sigma.shape[0]
            retardedeq[:l, :l] = retardedeq[:l, :l]-sigma
            tau_left = self.GetComplexEnergy()*Dagger(self.GetInternal_S_intl())
            tau_left -= Dagger(self.GetInternal_H_intl())
            A = LinearAlgebra.solve(retardedeq, tau_left)
            del retardedeq
            del tau_left
            tau_left = self.GetComplexEnergy() * self.GetInternal_S_intl() - \
                       self.GetInternal_H_intl()
            self.SetLeftLeadSelfEnergy(np.dot(tau_left, A))

    def GetLeftLeadSelfEnergy(self):
        # If the presentation is not set: update
        if not hasattr(self, "sigmaleft"):
            self.UpdateLeftLeadSelfEnergy()
        sigmaleft = self.sigmaleft
        # If not ReferenceSelfEnergies: Delete the self energy
        if not self.ReferenceSelfEnergies():
            self.UnregisterLeftLeadSelfEnergy()
        return sigmaleft

    def SetLeftLeadSelfEnergy(self, selfenergy):
        self.sigmaleft = selfenergy

    def UnregisterLeftLeadSelfEnergy(self):
        # Try to unregister left lead self energy:
        if hasattr(self, "sigmaleft") and (not hasattr(self, "widebandlimit")):
            delattr(self, "sigmaleft")

    def GetRetardedEquation(self):
        # Calculating the Scroedinger eq: E*O-H-sigmaL-sigmaR:
        H = self.GetComplexEnergy()*self.GetIdentityRepresentation()
        H = H-self.GetHamiltonianRepresentation()
        # E*I-H-sigmaL
        # Get left lead selfenergy
        leftleadselfenergy = self.GetLeftLeadSelfEnergy()
        H = H-leftleadselfenergy
        # E*I-H-sigmaL-sigmaR
        rightleadselfenergy = self.GetRightLeadSelfEnergy()
        H = H-rightleadselfenergy
        return H

    def UpdateRepresentation(self):
        """G(E) = inv(E*O-H-sigmaL-sigmaR)
        greenfunction = ArrayTools.inv(self.GetRetardedEquation(), copymatrix=0)"""
        greenfunction = LinearAlgebra.inv(self.GetRetardedEquation())
        self.SetRepresentation(greenfunction)

class NonEqNonIntGreenFunction(ScatteringGreenFunction):
    def __init__(self,
                 scatteringhamiltonian,
                 leftleadhamiltonian,
                 rightleadhamiltonian,
                 internalscatteringhamiltonian=None,
                 E_Fermi=0.0,
                 uncoupled=0):
        ScatteringGreenFunction.__init__(self,
                                         scatteringhamiltonian,
                                         leftleadhamiltonian,
                                         rightleadhamiltonian,
                                         internalscatteringhamiltonian)
        self.SetFermiLevel(E_Fermi)
        # 'uncoupled' is a flag that can be used to perform calculations for
        # an isolated central region, i.e. without coupling to leads. In this
        # case the Fermi level is the chemical potential of the central region.
        self.SetCouplingStatus(uncoupled)
        self.SetCouplingReductionFactor(1.0)

    def SpinPolarized(self):
        """The noninteracting GF is assumed to be
        non-spin polarized"""
        return 0

    def SetCouplingReductionFactor(self, factor):
        self.coupred = factor

    def GetCouplingReductionFactor(self):
        return self.coupred

    def SetTemperature(self, temp):
        """NOTE: If this is a GF pertaining to a GW GF
        you must also set the temperature of the GW GF."""
        self.temp = temp

    def GetTemperature(self):
        if not hasattr(self, 'temp'):
            self.temp = 0.0
        return self.temp

    def SetCouplingStatus(self, uncoupled):
        self.uncoupled = uncoupled

    def Uncoupled(self):
        return self.uncoupled

    def Equilibrium(self):
        return abs(self.GetLeftChemicalPotential() -
                   self.GetRightChemicalPotential()) < 1.0e-6

    def SetFermiLevel(self, efermi):
        """NOTE: If this is a GF pertaining to a GW GF
        you must also set the Fermi energy of the GW GF."""
        self.ResetCalculatedLists()
        self.efermi = efermi

    def GetFermiLevel(self):
        return self.efermi

    def GetLeftEnergy(self, energy):
        #return energy
        return energy-self.GetLeftChemicalPotential()

    def GetRightEnergy(self, energy):
        #return energy
        return energy-self.GetRightChemicalPotential()

    def SetHamiltonianRepresentation(self, h0):
        ScatteringGreenFunction.SetHamiltonianRepresentation(self, h0)
        self.ResetRetardedLesserCalculatedLists()

    def RetardedLesserCalculated(self):
        # List indicating which retarded and lesser GFs have been calculated
        return self.retlesscalc

    def RetardedSELeftCalculated(self):
        # List indicating which left retarded self-energies have been
        # calculated
        return self.retseleftcalc

    def RetardedSERightCalculated(self):
        # List indicating which right retarded self-energies have been
        # calculated
        return self.retserightcalc

    def GetCurrentIntegrationGrid(self):
        if self.GetTemperature() > 1.0e-3:
            return self.GetEnergyList()
        else:
            mu_l = self.GetLeftChemicalPotential()
            mu_r = self.GetRightChemicalPotential()
            efermi = self.GetFermiLevel()
            upper_bound = max(mu_l + efermi, mu_r + efermi)
            lower_bound = min(mu_l + efermi, mu_r + efermi)
            energies = self.GetEnergyList()
            start = np.searchsorted(energies, lower_bound)
            start = start - 2
            end = np.searchsorted(energies, upper_bound)+2
            return energies[start:end]


    def ResetCalculatedLists(self):
        """Calculated lists are reset"""
        self.retseleftcalc = np.zeros([len(self.GetEnergyList())],
                                      np.int)
        self.retserightcalc = np.zeros([len(self.GetEnergyList())],
                                       np.int)
        self.retlesscalc = np.zeros([len(self.GetEnergyList())],
                                    np.int)

    def ResetSECalculatedLists(self):
        """SE calculated lists are reset"""
        self.retseleftcalc = np.zeros([len(self.GetEnergyList())],
                                      np.int)
        self.retserightcalc = np.zeros([len(self.GetEnergyList())],
                                       np.int)

    def ResetRetardedLesserCalculatedLists(self):
        """Retarded/lesser calculated lists are reset"""
        self.retlesscalc = np.zeros([len(self.GetEnergyList())],
                                    np.int)

    def GetOccupations(self):
        occ_en = -1.0j * np.diagonal(self.GetLesserList(), axis1=-2, axis2=-1)
        de = self.GetEnergyList()[1] - self.GetEnergyList()[0]
        occupations = FFT.fft(occ_en, axis=0)[0]  *de / (2 * np.pi)
        return occupations

    def GetDensityMatrix(self):
        den_en = -1.0j * self.GetLesserList()
        de = self.GetEnergyList()[1] - self.GetEnergyList()[0]
        den = FFT.fft(den_en, axis=0)[0] * de / (2 * np.pi)
        return den

    def GetNaturalOrbitals(self):
        dm = self.GetDensityMatrix()
        dm = dm / (np.sum(np.diagonal(dm)))
        val, vec = eigenvectors(dm)
        ind = np.argsort(val.real)
        val = np.take(val.real, ind)
        vec = np.take(vec, ind)
        return val, vec

    def GetRetardedAndLesser(self, energy):
        if energy in self.GetEnergyList():
            index = self.GetEnergyIndex(energy)

            # Has it been calculated before?
            if self.RetardedLesserCalculated()[index] == 1:
                # Just pick the GFs from the pre-calculated lists
                return self.GetRetardedList()[index], \
                       self.GetLesserList()[index]

        # Calculate them...
        sigma_left = self.GetLeftLeadRetardedSelfEnergy(energy)
        sigma_right = self.GetRightLeadRetardedSelfEnergy(energy)

        # Set the energy in the GF
        self.SetEnergy(energy)

        # Calculate retarded GF
        H = self.GetComplexEnergy()*self.GetIdentityRepresentation()
        H = H - self.GetHamiltonianRepresentation()
        Gr = LinearAlgebra.inv(H - sigma_left - sigma_right)

        # The following holds only when T=0. In that case:
        # -G^<(e) = G^r(e) - G^a(e) for e < min{mu_L, mu_R}
        # -G^<(e) = G^r(e)[ sigma_L^r - sigma_L^a ] G^a(e) for mu_R < e < mu_L
        # -G^<(e) = G^r(e)[ sigma_R^r - sigma_R^a ] G^a(e) for mu_L < e < mu_R
        # -G^<(e) = 0 for e > max{mu_L, mu_R}          ,
        # In the finite T case (which is NOT implemented) the Keldysh equation
        # must be used:
        #   G^<(e) = G^r(e)[ - f_L(e){sigma_L^r - sigma_L^a}
        #                    - f_R(e){sigma_R^r - sigma_R^a}] G^a(e)
        mult = np.dot
        ef = self.GetFermiLevel()
        mul = self.GetLeftChemicalPotential()
        mur = self.GetRightChemicalPotential()
        T = self.GetTemperature()
        if self.Uncoupled() or self.Equilibrium():
            Gl = -FermiDistribution(energy - ef, kBT=T) * (Gr - Dagger(Gr))
        else:
            # Only correct for T = 0!
            if T < 1.0e-3 and (energy < min(mul + ef, mur + ef)
                               or energy > max(mul + ef, mur + ef)):
                if energy < min(mul + ef, mur + ef):
                    Gl = -(Gr - Dagger(Gr))
                else:
                    Gl = np.zeros(Gr.shape, np.complex)
            else:
                temp = mult(Gr, self.GetLeftLeadLesserSelfEnergy(energy) +
                            self.GetRightLeadLesserSelfEnergy(energy))
                Gl = mult(temp, Dagger(Gr))
                # Calculating the "extra" term in the Keldysh equation.
                # This is important in particular when bound states are formed
                # outside the band of the leads.
                # We use that (1+G^rSigma^R)G_0^l(1+Sigma^aG^a) = 2i\eta G^r G^a
                # The 'extra' term can introduce artificial kinks in G^l at E_F
                # due to the Fermi function.
                # This kink will vanish when eta -> 0.

                #extra1 = np.identity(Gr.shape[0], np.complex) \
                #         + mult(Gr, sigma_left+sigma_right)
                #extra2 = Dagger(extra1)
                #self.SetEnergy(energy)
                #G_0_ret = LinearAlgebra.inv(
                #    self.GetComplexEnergy() * self.GetIdentityRepresentation()
                #    - self.GetHamiltonianRepresentation())
                #G_0_less = -(G_0_ret - Dagger(G_0_ret)) * \
                #           FermiDistribution(energy - ef, T)
                #extra = mult(mult(extra1, G_0_less), extra2)
                extra = 2.0j * self.GetInfinitesimal() * \
                        FermiDistribution(energy - ef, T) * mult(Gr, Dagger(Gr))
                Gl = Gl + extra

        # Store GFs in lists if energy if necessary
        if energy in self.GetEnergyList():
            index = self.GetEnergyIndex(energy)
            self.SetRetardedListElement(index, Gr)
            self.SetLesserListElement(index, Gl)
        return Gr, Gl

    def GetGreater(self, energy):
        """Use: G^r - G^a = G^> - G^<"""
        Gr, Gl = self.GetRetardedAndLesser(energy)
        # Make sure that G^> vanishes below the lowest chemical potential
        # This is commented out because in sc calculations the GF is an
        # interacting one for which this is not necessarily true!
#       if self.GetTemperature()<1.0e-3:
#           mu=min(self.GetLeftChemicalPotential(),
#                      self.GetRightChemicalPotential())
#           pre=(np.sign(energy-(self.GetFermiLevel()+mu))+1.0)*0.5
#       else:
#           pre=1.0
#       return pre*(Gl+Gr-Dagger(Gr))
        return Gl + Gr - Dagger(Gr)

    def GetLeftLeadRetardedSelfEnergy(self, energy):
        if self.Uncoupled():
            return np.zeros(self.GetIdentityRepresentation().shape,
                            np.complex)
        if energy in self.GetEnergyList():
            index = self.GetEnergyIndex(energy)
            if self.RetardedSELeftCalculated()[index]:
                return self.GetRetardedSEListLeft()[index]
        # Calculate it...
        self.SetEnergy(self.GetLeftEnergy(energy))
        sigma_left = self.GetLeftLeadSelfEnergy() *\
                     self.GetCouplingReductionFactor()
        # Store the self-energies if in the list
        if energy in self.GetEnergyList():
            index = self.GetEnergyIndex(energy)
            self.SetRetardedSEListLeft(index, sigma_left)
            # Set flag indicating that it has now been calculated
            self.RetardedSELeftCalculated()[index] = 1
        return sigma_left

    def GetLeftLeadLesserSelfEnergy(self, energy):
        """Calculates sigma^<(e) = -f(e)[sigma^r - sigma^a]"""
        abs_leftmu = self.GetFermiLevel()+self.GetLeftChemicalPotential()
        f = FermiDistribution(energy=energy-abs_leftmu,
                              kBT=self.GetTemperature())
        sigma_left = self.GetLeftLeadRetardedSelfEnergy(energy)
        return 1.0j*f*LambdaFromSelfEnergy(sigma_left)

    def GetLeftLeadGreaterSelfEnergy(self, energy):
        """Calculates sigma^>(e) = [1-f(e)][sigma^r - sigma^a]"""
        abs_leftmu = self.GetFermiLevel()+self.GetLeftChemicalPotential()
        f = FermiDistribution(energy=energy-abs_leftmu, kBT=self.GetTemperature())
        sigma_left = self.GetLeftLeadRetardedSelfEnergy(energy)
        return 1.0j*(f-1)*LambdaFromSelfEnergy(sigma_left)

    def GetRightLeadRetardedSelfEnergy(self, energy):
        if self.Uncoupled():
            return np.zeros(self.GetIdentityRepresentation().shape, np.complex)
        if energy in self.GetEnergyList():
            index = self.GetEnergyIndex(energy)
            if self.RetardedSERightCalculated()[index]:
                return self.GetRetardedSEListRight()[index]
        # Calculate it...
        self.SetEnergy(self.GetRightEnergy(energy))
        sigma_right = self.GetRightLeadSelfEnergy()*self.GetCouplingReductionFactor()
        # Store the self-energies if in the list
        if energy in self.GetEnergyList():
            index = self.GetEnergyIndex(energy)
            self.SetRetardedSEListRight(index, sigma_right)
            # Set flag indicating that it has now been calculated
            self.RetardedSERightCalculated()[index] = 1
        return sigma_right

    def GetRightLeadLesserSelfEnergy(self, energy):
        """Calculates sigma^<(e) = -f(e)[sigma^r - sigma^a]"""
        abs_rightmu = self.GetFermiLevel()+self.GetRightChemicalPotential()
        f = FermiDistribution(energy=energy-abs_rightmu, kBT=self.GetTemperature())
        sigma_right = self.GetRightLeadRetardedSelfEnergy(energy)
        return 1.0j*f*LambdaFromSelfEnergy(sigma_right)

    def GetRightLeadGreaterSelfEnergy(self, energy):
        """Calculates sigma^>(e) = [1-f(e)][sigma^r - sigma^a]"""
        abs_rightmu = self.GetFermiLevel()+self.GetRightChemicalPotential()
        f = FermiDistribution(energy=energy-abs_rightmu, kBT=self.GetTemperature())
        sigma_right = self.GetRightLeadRetardedSelfEnergy(energy)
        return 1.0j*(f-1)*LambdaFromSelfEnergy(sigma_right)

    def CalculateRetardedLesserLists(self):
        """Calculating retarded/lesser lists"""
        for index, en in enumerate(self.GetEnergyList()):
            if not self.RetardedLesserCalculated()[index]:
                # The GFs are automatically stored in the list when calculated
                gr, gl = self.GetRetardedAndLesser(en)

    def CalculateRetardedSELeftList(self):
        """Calculating left retarded list"""
        for index, en in enumerate(self.GetEnergyList()):
            if not self.RetardedSELeftCalculated()[index]:
                # The SE is automatically stored in the list when calculated
                sigma = self.GetLeftLeadRetardedSelfEnergy(en)
                self.RetardedSELeftCalculated()[index] = 1

    def CalculateRetardedSERightList(self):
        """Calculating right retarded list"""
        for index, en in enumerate(self.GetEnergyList()):
            if not self.RetardedSERightCalculated()[index]:
                #The SE is automatically stored in the list when calculated
                sigma = self.GetRightLeadRetardedSelfEnergy(en)
                self.RetardedSERightCalculated()[index] = 1

    def SetLeftChemicalPotential(self, mul):
        """Chemical potential is wrt. Fermi level
        NOTE: If this is a GF pertaining to a GW GF
        you must also set the chem. pot. of the GW GF."""
        self.mul = self.GetNearestEnergyMidpoint(mul)
        self.ResetCalculatedLists()

    def GetLeftChemicalPotential(self):
        return self.mul

    def SetRightChemicalPotential(self, mur):
        """ Chemical potential is wrt. Fermi level
        NOTE: If this is a GF pertaining to a GW GF
        you must also set the chem. pot. of the GW GF."""
        self.mur = self.GetNearestEnergyMidpoint(mur)
        self.ResetCalculatedLists()

    def GetRightChemicalPotential(self):
        return self.mur

    def SetEnergyList(self, energylist):
        """List of energies for which the
        retarded and lesser GFs are kept
        round off energies to avoid errors
        when comparing to other lists
        NOTE: If this is a GF pertaining to a GW GF
        you must also set the chem. pot. of the GW GF."""
        energylist = np.around(energylist, 7)
        Nel = len(energylist)

        if Nel == len(self.GetEnergyList()) and \
           max(abs(self.GetEnergyList() - energylist)) < 1.0e-7:
            # The old energy list is the same as the new,
            # and nothing happens
            return

        self.rlenergylist = energylist

        # Initialize lists to hold the GFs and self-energies
        dim = (Nel, ) + self.GetHamiltonianRepresentation().shape
        self.retardedlist = np.zeros(dim, np.complex)
        self.lesserlist = np.zeros(dim, np.complex)
        self.retlesscalc = np.zeros(Nel, np.int)
        self.selistleft = np.zeros(dim, np.complex)
        self.selistright = np.zeros(dim, np.complex)
        self.retseleftcalc = np.zeros(Nel, np.int)
        self.retserightcalc = np.zeros(Nel, np.int)

        # Adjust chemical potentials if needed
        if hasattr(self, 'mul'): self.SetLeftChemicalPotential(self.mul)
        if hasattr(self, 'mur'): self.SetRightChemicalPotential(self.mur)

    def GetEnergyList(self):
        if not hasattr(self, 'rlenergylist'):
            self.rlenergylist = []
        return self.rlenergylist

    def GetEnergyIndex(self, energy):
        return self.GetEnergyList().tolist().index(energy)

    def SetRetardedListElement(self, index, value):
        self.RetardedLesserCalculated()[index] = 1
        self.retardedlist[index] = value

    def SetRetardedList(self, lst):
        self.retardedlist = lst
        self.RetardedLesserCalculated()[:] = [1] * len(
            self.RetardedLesserCalculated())

    def GetRetardedList(self):
        """Test if all GFs have been calculated"""
        if np.sum(self.RetardedLesserCalculated()) < \
               len(self.GetEnergyList()):
            self.CalculateRetardedLesserLists()
        return self.retardedlist

    def GetLesserList(self):
        """Test if all GFs have been calculated"""
        if np.sum(self.RetardedLesserCalculated()) < \
               len(self.GetEnergyList()):
            self.CalculateRetardedLesserLists()
        return self.lesserlist

    def SetLesserListElement(self, index, value):
        self.RetardedLesserCalculated()[index] = 1
        self.lesserlist[index] = value

    def SetLesserList(self, lst):
        self.lesserlist = lst
        self.RetardedLesserCalculated()[:] = [1] * len(
            self.RetardedLesserCalculated())

    def GetGreaterList(self):
        """Assuming that retarded and lesser lists have been calculated
        Use: G^r - G^a = G^> - G^<"""
        Gr = self.GetRetardedList()
        Ga = np.conjugate(np.swapaxes(Gr, 1, 2))
        Gl = self.GetLesserList()
        Gg = Gl+Gr-Ga
        # The lines are commented out. See 'GetGreater' for explanation.
#       if self.GetTemperature()<1.0e-3:
#           dim = Gg.shape
#           mu  =  min(self.GetLeftChemicalPotential(),
#                    self.GetRightChemicalPotential())
#           energies = self.GetEnergyList()
#           pre = (np.sign(energies-(self.GetFermiLevel()+mu))+1.0)*0.5
#           pre = np.reshape(pre, [dim[0], 1, 1])
#           pre = np.repeat(pre, repeats=dim[1], axis=1)
#           pre = np.repeat(pre, repeats=dim[1], axis=2)
#           Gg = Gg*pre
        return Gg

    def GetAdvancedList(self):
        Gr = self.GetRetardedList()
        return np.conjugate(np.swapaxes(Gr, 1, 2))


    def GetRetardedSEListLeft(self):
        # Test if all SEs have been calculated
        if np.sum(self.RetardedSELeftCalculated()) < \
               len(self.GetEnergyList()):
            self.CalculateRetardedSELeftList()
        return self.selistleft

    def SetRetardedSEListLeft(self, index, value):
        self.selistleft[index] = value

    def GetRetardedSEListRight(self):
        # Test if all SEs have been calculated
        if np.sum(self.RetardedSERightCalculated()) < \
               len(self.GetEnergyList()):
            self.CalculateRetardedSERightList()
        return self.selistright

    def SetRetardedSEListRight(self, index, value):
        self.selistright[index] = value

    def GetNearestEnergyMidpoint(self, energy):
        energies = self.GetEnergyList()
        if energies == []: return energy
        if len(energies) == 1: return energies[0]
        i = 0
        while energy > energies[i] and i < len(energies) - 1: i += 1
        return .5 * (energies[i] + energies[i + 1])


class NonEqIntGreenFunction(NonEqNonIntGreenFunction):
    """ This class inherits from NonEqNonIntGreenFunction. However, most
        methods are re-written here so that they refer to the instance of
        NonEqNonIntGreenFunction kept by the interacting self-energy.
        All the list stuff in NonEqNonIntGreenFunction is not implemented
        for this class.
    """

    def __init__(self, listofintselfenergies):
        self.SetListOfInteractingSelfEnergies(listofintselfenergies)

    def SpinPolarized(self):
        return 0

    def GetTemperature(self):
        return self.GetNonIntGreenFunction().GetTemperature()

    def GetMatrixDimension(self):
        return self.GetNonIntGreenFunction().GetMatrixDimension()

    def GetActiveList(self):
        lst = []
        for se in self.GetListOfInteractingSelfEnergies():
            lst.append(se.Active())
        return lst

    def CheckActiveList(self):
        names = self.GetNamesOfInteractingSelfEnergies()
        if ('GW' in names) and ('fock' in names):
            if self.GetSelfEnergy('fock').Active() and \
                   self.GetSelfEnergy('GW').Active():
                print("**************************************************")
                print("WARNING: Both GW and Fock self-energies are active")
                print("**************************************************")

    def SetActiveList(self, lst):
        for i in range(len(self.GetListOfInteractingSelfEnergies())):
            self.GetListOfInteractingSelfEnergies()[i].SetActive(lst[i])

    def UpdateInteractingSelfEnergies(self, conv):
        for se in self.GetListOfInteractingSelfEnergies():
            if se.Active():
                se.Update(conv)

    def UnregisterInteractingSelfEnergies(self):
        for se in self.GetListOfInteractingSelfEnergies():
            se.Unregister()

    def SetTemperature(self, T):
        self.GetNonIntGreenFunction().SetTemperature(T)
        #if hasattr(self, 'keldyshbounds'):
        #    del self.keldyshbounds
        if 'GW' in self.GetNamesOfInteractingSelfEnergies():
            index = self.GetNamesOfInteractingSelfEnergies().index('GW')
            self.GetListOfInteractingSelfEnergies()[index].EvaluateCurrentGrid()
        self.UnregisterGWLists()
        self.UnregisterInteractingSelfEnergies()

    def GetEnergyList(self):
        return self.GetNonIntGreenFunction().GetEnergyList()

    def SetEnergyList(self, en):
        self.GetNonIntGreenFunction().SetEnergyList(en)
        if 'GW' in self.GetNamesOfInteractingSelfEnergies():
            self.GetSelfEnergy('GW').UnregisterExtendedEnergyList()

    def GetCurrentIntegrationGrid(self):
        if 'GW' in self.GetNamesOfInteractingSelfEnergies():
            index = self.GetNamesOfInteractingSelfEnergies().index('GW')
            return self.GetListOfInteractingSelfEnergies()[index].GetCurrentGrid()
        return self.GetNonIntGreenFunction().GetCurrentIntegrationGrid()

    def SetListOfInteractingSelfEnergies(self, listofintselfenergies):
        self.listofintselfenergies = listofintselfenergies

    def GetListOfInteractingSelfEnergies(self):
        return self.listofintselfenergies

    def GetNamesOfInteractingSelfEnergies(self):
        names = []
        for se in self.GetListOfInteractingSelfEnergies():
            names.append(se.GetName())
        return names

    def GetSelfEnergy(self, name):
        names = self.GetNamesOfInteractingSelfEnergies()
        ind = names.index(name)
        return self.GetListOfInteractingSelfEnergies()[ind]

    def RemoveSelfEnergy(self, name):
        names = self.GetNamesOfInteractingSelfEnergies()
        ind = names.index(name)
        lst = self.GetListOfInteractingSelfEnergies()
        lst[ind:ind+1] = []
        self.SetListOfInteractingSelfEnergies(lst)

    def GetRetardedInteractingSelfEnergy(self, energy, conv=False):
        lst = self.GetListOfInteractingSelfEnergies()
        se = np.zeros(self.GetMatrixDimension(), np.complex)
        for i in range(len(self.GetListOfInteractingSelfEnergies())):
            if lst[i].Active():
                se = se+lst[i].GetRetarded(energy, conv)
        return se

    def GetRetardedSelfEnergy(self, energy, GW_conv=False):
        GF_0 = self.GetNonIntGreenFunction()
        ret_int = self.GetRetardedInteractingSelfEnergy(energy, GW_conv)
        ret_left = GF_0.GetLeftLeadRetardedSelfEnergy(energy)
        ret_right = GF_0.GetRightLeadRetardedSelfEnergy(energy)
        return ret_int+ret_left+ret_right

    def GetLesserInteractingSelfEnergy(self, energy):
        lst = self.GetListOfInteractingSelfEnergies()
        se = np.zeros(self.GetMatrixDimension(), np.complex)
        for lst_i in lst:
            if lst_i.Active():
                se = se + lst_i.GetLesser(energy)
        return se

    def GetLesserSelfEnergy(self, energy):
        GF_0 = self.GetNonIntGreenFunction()
        less_int = self.GetLesserInteractingSelfEnergy(energy)
        less_left = GF_0.GetLeftLeadLesserSelfEnergy(energy)
        less_right = GF_0.GetRightLeadLesserSelfEnergy(energy)
        return less_int+less_left+less_right

    def SetHamiltonianRepresentation(self, h0):
        self.GetNonIntGreenFunction().SetHamiltonianRepresentation(h0)
        self.UnregisterGWLists()

    def GetNonIntGreenFunction(self):
        return self.GetListOfInteractingSelfEnergies()[0].GetNonIntGreenFunction()

    def SetFermiLevel(self, efermi):
        self.GetNonIntGreenFunction().SetFermiLevel(efermi)
        if 'GW' in self.GetNamesOfInteractingSelfEnergies():
            index = self.GetNamesOfInteractingSelfEnergies().index('GW')
            self.GetListOfInteractingSelfEnergies()[index].EvaluateCurrentGrid()
        #for se in self.GetListOfInteractingSelfEnergies():
        #   se.SetUpdate()
        self.UnregisterGWLists()
        self.UnregisterInteractingSelfEnergies()

    def GetFermiLevel(self):
        return self.GetNonIntGreenFunction().GetFermiLevel()

    def SetLeftChemicalPotential(self, mul):
    # Chemical potential is wrt. Fermi level
        self.GetNonIntGreenFunction().SetLeftChemicalPotential(mul)
        if 'GW' in self.GetNamesOfInteractingSelfEnergies():
            index = self.GetNamesOfInteractingSelfEnergies().index('GW')
            self.GetListOfInteractingSelfEnergies()[index].EvaluateCurrentGrid()
        #for se in self.GetListOfInteractingSelfEnergies():
        #   se.SetUpdate()
        self.UnregisterGWLists()
        self.UnregisterInteractingSelfEnergies()

    def GetLeftChemicalPotential(self):
        return self.GetNonIntGreenFunction().GetLeftChemicalPotential()

    def SetRightChemicalPotential(self, mur):
    # Chemical potential is wrt. Fermi level
        self.GetNonIntGreenFunction().SetRightChemicalPotential(mur)
        if 'GW' in self.GetNamesOfInteractingSelfEnergies():
            index = self.GetNamesOfInteractingSelfEnergies().index('GW')
            self.GetListOfInteractingSelfEnergies()[index].EvaluateCurrentGrid()
        #for se in self.GetListOfInteractingSelfEnergies():
        #   se.SetUpdate()
        self.UnregisterGWLists()
        self.UnregisterInteractingSelfEnergies()

    def GetRightChemicalPotential(self):
        return self.GetNonIntGreenFunction().GetRightChemicalPotential()



    def UpdateRetardedAndLesserLists(self, conv=False):
        energies = self.GetEnergyList()
        lst1 = []
        lst2 = []
        for en in energies:
            gr, gl = self.GetRetardedAndLesser(en, conv)
            lst1.append(gr)
            lst2.append(gl)
        self.SetRetardedList(np.array(lst1))
        self.SetLesserList(np.array(lst2))

    def SetRetardedList(self, lst):
        self.retlst = lst

    def SetLesserList(self, lst):
        self.lesslst = lst

    def UnregisterGWLists(self):
        if hasattr(self, 'retlst'):
            del self.retlst
        if hasattr(self, 'lesslst'):
            del self.lesslst

    def GetRetardedList(self):
        if not hasattr(self, 'retlst'):
            self.UpdateRetardedAndLesserLists()
        return self.retlst

    def GetLesserList(self):
        if not hasattr(self, 'lesslst'):
            self.UpdateRetardedAndLesserLists()
        return self.lesslst

    def GetKeldyshBounds(self):
        #return [0, 0]
        # Interval within which the Keldysh equation should be used for G^l.
        # Outside the fluctuation dissipation theorem can be used.
        en_cur = self.GetCurrentIntegrationGrid()
        return [en_cur[0]-1.0e-7, en_cur[-1]+1.0e-7]


    def GetRetardedAndLesser(self, energy, GW_conv=False):
        # By asking first for the interacting self-energy we make sure that lead self-
        # energies are not calculated twice.
        # If energy is not in the pre-defined energy list of the self-energy, an interpolation
        # is perfomed.
        # First get all the self-energies needed
        GF_0 = self.GetNonIntGreenFunction()
        sigma_ret = self.GetRetardedSelfEnergy(energy, GW_conv)
        #########################
        # Calculate retarded GF #
        #########################
        GF_0.SetEnergy(energy)
        H = GF_0.GetComplexEnergy()*GF_0.GetIdentityRepresentation()
        H = H-GF_0.GetHamiltonianRepresentation()
        H = H-sigma_ret
        Gr = LinearAlgebra.inv(H)
        bounds = self.GetKeldyshBounds()
        if not (GF_0.Uncoupled() or GF_0.Equilibrium()) and (bounds[0] <= energy <= bounds[1]):
        # For higher precision one could use the converged GW^r inside the small Keldysh interval
        #####################################################
        # Use Keldysh equation to obtain lesser/greater GFs #
        #####################################################
            sigma_less = self.GetLesserSelfEnergy(energy)
            mult = np.dot
            temp = mult(Gr, sigma_less)
            Gl = mult(temp, Dagger(Gr))
            # Calculating the "extra" term in the Keldysh equation if necessary:
            #extra1 = np.identity(Gr.shape[0], np.complex)+mult(Gr, sigma_ret)
            #extra2 = Dagger(extra1)
            #GF_0.SetEnergy(energy)
            #G_0_ret = LinearAlgebra.inv(GF_0.GetComplexEnergy()*
            #GF_0.GetIdentityRepresentation()
            #-GF_0.GetHamiltonianRepresentation())
            #G_0_less = -(G_0_ret-Dagger(G_0_ret))
            #*FermiDistribution(energy-GF_0.GetFermiLevel(),
            #kBT = self.GetTemperature())
            #extra = mult(mult(extra1, G_0_less), extra2)
            extra = (2.0j * GF_0.GetInfinitesimal()
                     * FermiDistribution(energy - GF_0.GetFermiLevel(),
                                         kBT=self.GetTemperature())*mult(Gr, Dagger(Gr)))
            Gl = Gl+extra
        else:
            # Use dissipation-fluctuation theorem
            Gl = -FermiDistribution(energy-GF_0.GetFermiLevel(),
                                    kBT=self.GetTemperature())*(Gr-Dagger(Gr))
        return Gr, Gl


    def GetOccupations(self):
        occ_en = -1.0j*np.diagonal(self.GetLesserList(), axis1=-2, axis2=-1)
        de = self.GetEnergyList()[1]-self.GetEnergyList()[0]
        occ = FFT.fft(occ_en, axis=0)[0]*de/(2*np.pi)
        return occ

    def GetDensityMatrix(self):
        den_en = -1.0j*self.GetLesserList()
        de = self.GetEnergyList()[1]-self.GetEnergyList()[0]
        den = FFT.fft(den_en, axis=0)[0]*de/(2*np.pi)
        return den

    def GetLeftLeadRetardedSelfEnergy(self, energy):
        return self.GetNonIntGreenFunction().GetLeftLeadRetardedSelfEnergy(energy)

    def GetRightLeadRetardedSelfEnergy(self, energy):
        return self.GetNonIntGreenFunction().GetRightLeadRetardedSelfEnergy(energy)

    def GetLeadRetardedSelfEnergy(self, energy):
        left = self.GetLeftLeadRetardedSelfEnergy(energy)
        right = self.GetRightLeadRetardedSelfEnergy(energy)
        return left+right

    def SelfConsistent(self,
                       mytype=None,
                       tol=0.01,
                       mix=0.1,
                       history=3,
                       temp=False,
                       V_step=False,
                       GW_conv=False):
        from eqe.transport import SelfConsistency
        self.CheckActiveList()
        if mytype is None:
            SelfConsistency.SelfConsistent(self, tol, mix, history, temp, GW_conv)
        if mytype == 'GW':
            SelfConsistency.SelfConsistentGWPulayMixGF(self, tol, mix, history, temp, GW_conv)
        if mytype == 'GW_V':
            SelfConsistency.SelfConsistentGWPulayMixGF_V(self, tol, mix, history, V_step, GW_conv)
        if mytype == 'G':
            SelfConsistency.SelfConsistentGPulayMixGF(self, tol, mix, history)
        if mytype == 'GWhartree':
            SelfConsistency.SelfConsistentGWPulayMixHartree(self, tol, mix, history)
        if mytype == 'hartree':
            SelfConsistency.SelfConsistentHartree(self, tol, mix, history)
        if mytype == 'hartree2':
            SelfConsistency.SelfConsistentHartree2(self, tol, mix, history)
        if mytype == 'occ':
            SelfConsistency.SelfConsistentOccupation(self, tol, mix)
        if mytype == 'occPulay':
            SelfConsistency.SelfConsistentOccupationPulay(self, tol, mix, history)
        if mytype == 'HF':
            SelfConsistency.SelfConsistentHF(self, tol, mix, history)


    def GetQuasiparticleEnergies(self):
        mult = np.dot

        self.CheckActiveList()
        # Start by finding "LDA" wavefunctions
        H = self.GetNonIntGreenFunction().GetHamiltonianRepresentation()
        val, vec = eigenvectors(H)
        indices = np.argsort(val.real)
        val = np.take(val.real, indices)
        print("Hartree energies:", val)
        vec = np.take(vec, indices)
        U = np.transpose(vec)

        energies = self.GetNonIntGreenFunction().GetEnergyList()
        de = energies[1]-energies[0]
        quasi = []
        for i in range(len(val)):
            # Find index corresponding to the e_i
            energy = val[i]+1.0e-8
            index = np.searchsorted(energies, energy)
            se_left = self.GetRetardedSelfEnergy(energies[index-1])
            se_0 = self.GetRetardedSelfEnergy(energies[index])
            se_right = self.GetRetardedSelfEnergy(energies[index+1])
            Z = (se_right-se_left)/(2*de)
            print("Z", Z)
            Z_i = 1.0/(1.0-RotateMatrix(Z, U)[i, i])
            se_i = RotateMatrix(se_0, U)[i, i]
            print("se_i", se_i)
            print("val", val[i])
            ei = val[i]+Z_i*se_i
            quasi.append(ei)
        return quasi


#####################################
# Below a spin-polarized version    #
#####################################

class NonEqIntGreenFunctionPolar(NonEqNonIntGreenFunction):
    """This class inherits from NonEqNonIntGreenFunction.
    However, most methods are re-written here so that
    they refer to the instance of NonEqNonIntGreenFunction
    kept by the interacting self-energy.
    All the list stuff in NonEqNonIntGreenFunction
    is not implemented for this class."""

    def __init__(self, listofintselfenergies):
        self.SetListOfInteractingSelfEnergies(listofintselfenergies)

    def SpinPolarized(self):
        return 1

    def GetTemperature(self):
        return self.GetNonIntGreenFunction()[0].GetTemperature()

    def SetTemperature(self, T):
        self.GetNonIntGreenFunction()[0].SetTemperature(T)
        self.GetNonIntGreenFunction()[1].SetTemperature(T)
        if hasattr(self, 'keldyshbounds'):
            del self.keldyshbounds
        index = self.GetNamesOfInteractingSelfEnergies().index('GW')
        self.GetListOfInteractingSelfEnergies()[index].EvaluateCurrentGrid()

    def GetRetardedSEEnergyList(self):
        return self.GetNonIntGreenFunction()[0].GetRetardedSEEnergyList()

    def GetCurrentIntegrationGrid(self):
        for se in self.GetListOfInteractingSelfEnergies():
            # Find the GW self energy and return the current grid
            if se.GetName() == 'GW':
                return se.GetCurrentGrid()

    def SetListOfInteractingSelfEnergies(self, listofintselfenergies):
        self.listofintselfenergies = listofintselfenergies

    def GetListOfInteractingSelfEnergies(self):
        return self.listofintselfenergies

    def GetNamesOfInteractingSelfEnergies(self):
        names = []
        for se in self.GetListOfInteractingSelfEnergies():
            names.append(se.GetName())
        return names

    def GetRetardedInteractingSelfEnergy(self, energy, spin, conv=False):
        se = self.GetListOfInteractingSelfEnergies()[0].GetRetarded(energy=energy,
                                                                    spin=spin,
                                                                    converged=conv)
        for i in range(1,
                       len(self.GetListOfInteractingSelfEnergies())):
            se = se+self.GetListOfInteractingSelfEnergies()[i].GetRetarded(energy=energy,
                                                                           spin=spin,
                                                                           converged=conv)
        return se

    def GetRetardedSelfEnergy(self, energy, spin, GW_conv=False):
        ret_int = self.GetRetardedInteractingSelfEnergy(energy=energy, spin=spin, conv=GW_conv)
        ret_lead = self.GetLeadRetardedSelfEnergy(energy=energy, spin=spin)
        return ret_int+ret_lead

    def GetLesserInteractingSelfEnergy(self, energy, spin):
        se = self.GetListOfInteractingSelfEnergies()[0].GetLesser(energy, spin)
        for i in range(1, len(self.GetListOfInteractingSelfEnergies())):
            se = se+self.GetListOfInteractingSelfEnergies()[i].GetLesser(energy, spin)
        return se

    def GetLesserSelfEnergy(self, energy, spin):
        less_int = self.GetLesserInteractingSelfEnergy(energy, spin)
        less_lead = self.GetLeadLesserSelfEnergy(energy, spin)
        return less_int+less_lead

    def SetHamiltonianRepresentation(self, h0, spin):
        self.GetNonIntGreenFunction()[spin].SetHamiltonianRepresentation(h0)

    def GetNonIntGreenFunction(self):
        return self.GetListOfInteractingSelfEnergies()[0].GetNonIntGreenFunction()

    def SetFermiLevel(self, efermi):
        self.GetNonIntGreenFunction()[0].SetFermiLevel(efermi)
        self.GetNonIntGreenFunction()[1].SetFermiLevel(efermi)
        for se in self.GetListOfInteractingSelfEnergies():
            se.UnregisterAll()

    def GetFermiLevel(self):
        return self.GetNonIntGreenFunction()[0].GetFermiLevel()

    def SetLeftChemicalPotential(self, mul):
    # Chemical potential is wrt. Fermi level
        self.GetNonIntGreenFunction()[0].SetLeftChemicalPotential(mul)
        self.GetNonIntGreenFunction()[1].SetLeftChemicalPotential(mul)
        for se in self.GetListOfInteractingSelfEnergies():
            se.UnregisterAll()

    def GetLeftChemicalPotential(self):
        return self.GetNonIntGreenFunction()[0].GetLeftChemicalPotential()

    def SetRightChemicalPotential(self, mur):
    # Chemical potential is wrt. Fermi level
        self.GetNonIntGreenFunction()[0].SetRightChemicalPotential(mur)
        self.GetNonIntGreenFunction()[1].SetRightChemicalPotential(mur)
        for se in self.GetListOfInteractingSelfEnergies():
            se.UnregisterAll()

    def GetRightChemicalPotential(self):
        return self.GetNonIntGreenFunction()[0].GetRightChemicalPotential()

    def UpdateRetardedAndLesserLists(self):
        energies = self.GetRetardedSEEnergyList()
        ret = []
        less = []
        for spin in range(2):
            lst1 = []
            lst2 = []
            for en in energies:
                gr, gl = self.GetRetardedAndLesser(en, spin)
                lst1.append(gr)
                lst2.append(gl)
            ret.append(np.array(lst1))
            less.append(np.array(lst2))
        self.SetRetardedList(ret)
        self.SetLesserList(less)

    def SetRetardedList(self, lst):
        self.retlst = lst

    def SetLesserList(self, lst):
        self.lesslst = lst

    def GetRetardedList(self):
        if not hasattr(self, 'retlst'):
            self.UpdateRetardedAndLesserLists()
        return self.retlst

    def GetLesserList(self):
        if not hasattr(self, 'lesslst'):
            self.UpdateRetardedAndLesserLists()
        return self.lesslst

    def GetKeldyshBounds(self):
        # Interval within which the Keldysh equation should be used for G^l.
        # Outside the fluctuation dissipation theorem can be used.
        # We empirically enlarge the interval by 10*T for finite temperatures.
        T = self.GetTemperature()
        if not hasattr(self, 'keldyshbounds'):
            mu_l = self.GetNonIntGreenFunction()[0].GetLeftChemicalPotential()
            mu_r = self.GetNonIntGreenFunction()[0].GetRightChemicalPotential()
            efermi = self.GetNonIntGreenFunction()[0].GetFermiLevel()
            upper_bound = max(mu_l+efermi, mu_r+efermi)+abs(mu_l)+abs(mu_r)+10*T
            lower_bound = min(mu_l+efermi, mu_r+efermi)-abs(mu_l)-abs(mu_r)-10*T
            self.keldyshbounds = [lower_bound, upper_bound]
        return self.keldyshbounds

    def GetRetardedAndLesser(self, energy, spin=0, GW_conv=False):
        """By asking first for the interacting self-energy
        we make sure that lead self-energies are
        not calculated twice.
        If energy is not in the pre-defined energy list
        of the self-energy, an interpolation is perfomed.
        First get all the self-energies needed."""
        GF_0 = self.GetNonIntGreenFunction()[spin]
        sigma_ret = self.GetRetardedSelfEnergy(energy, spin, GW_conv)
        #########################
        # Calculate retarded GF #
        #########################
        GF_0.SetEnergy(energy)
        H = GF_0.GetComplexEnergy()*GF_0.GetIdentityRepresentation()
        H = H-GF_0.GetHamiltonianRepresentation()
        H = H-sigma_ret
        Gr = LinearAlgebra.inv(H)
        bounds = self.GetKeldyshBounds()
        if not GF_0.Uncoupled() and (bounds[0] <= energy <= bounds[1]):
        # For higher precision one could use the converged GW^r inside the small Keldysh interval
        #####################################################
        # Use Keldysh equation to obtain lesser/greater GFs #
        #####################################################
            sigma_less = self.GetLesserSelfEnergy(energy, spin)
            mult = np.dot
            temp = mult(Gr, sigma_less)
            Gl = mult(temp, Dagger(Gr))
            # Calculating the "extra" term in the Keldysh equation if necessary:
            if GF_0.GetInfinitesimal() > 1.0e-5:
                extra1 = np.identity(Gr.shape[0], np.complex)+mult(Gr, sigma_ret)
                extra2 = Dagger(extra1)
                GF_0.SetEnergy(energy)
                G_0_ret = LinearAlgebra.inv((GF_0.GetComplexEnergy()
                                             *GF_0.GetIdentityRepresentation()
                                             -GF_0.GetHamiltonianRepresentation()))
                G_0_less = -((G_0_ret-Dagger(G_0_ret))
                             *FermiDistribution(energy-GF_0.GetFermiLevel(),
                                                kBT=self.GetTemperature()))
                extra = mult(mult(extra1, G_0_less), extra2)
                Gl = Gl+extra
        else:
            # Use dissipation-fluctuation theorem
            Gl = -(FermiDistribution(energy-GF_0.GetFermiLevel(),
                                     kBT=self.GetTemperature())
                   *(Gr-Dagger(Gr)))
        return Gr, Gl

    def GetLeftLeadRetardedSelfEnergy(self, energy, spin):
        return self.GetNonIntGreenFunction()[spin].GetLeftLeadRetardedSelfEnergy(energy)

    def GetRightLeadRetardedSelfEnergy(self, energy, spin):
        return self.GetNonIntGreenFunction()[spin].GetRightLeadRetardedSelfEnergy(energy)

    def GetLeadRetardedSelfEnergy(self, energy, spin):
        left = self.GetLeftLeadRetardedSelfEnergy(energy, spin)
        right = self.GetRightLeadRetardedSelfEnergy(energy, spin)
        return left+right

    def GetLeftLeadLesserSelfEnergy(self, energy, spin):
        return self.GetNonIntGreenFunction()[spin].GetLeftLeadLesserSelfEnergy(energy)

    def GetRightLeadLesserSelfEnergy(self, energy, spin):
        return self.GetNonIntGreenFunction()[spin].GetRightLeadLesserSelfEnergy(energy)

    def GetLeadLesserSelfEnergy(self, energy, spin):
        left = self.GetLeftLeadLesserSelfEnergy(energy, spin)
        right = self.GetRightLeadLesserSelfEnergy(energy, spin)
        return left+right

    def SelfConsistent(self, mytype, tol=0.01, mix=0.1, history=3, temp=False):
        from eqe.transport import SelfConsistency

        if mytype == 'GW':
            SelfConsistency.SelfConsistentGWPulayMixGFPolar(self, tol, mix, history, temp)
        if mytype == 'hartree':
            SelfConsistency.SelfConsistentHartreePolar(self, tol, mix, history)

class MultiLeadScatteringGreenFunction(ScatteringGreenFunction):
    """Multiple terminal implementation of the
    scattering green function code.
    Leads[0, n]    is an array containing all n
                   lead hamiltonians, n >= 2
    leadindices[0, n] is an array of the n
                       lead hamiltonian's start positions
    input          is an integer index in [0, n-1]
                   for the input lead
                   (default value is 0)
    output         is an integer index in [0, n-1]
                   for the output lead
                    (default value is 1)"""
    def __init__(self,
                 scatteringhamiltonian=None,
                 Leads=None,
                 input=0,
                 output=1,
                 internalscatteringhamiltonian=None,
                 energy=None,
                 DeltaS=0.001):
        self.SetLeadHamiltonians(Leads)
        self.SetLeftLeadIndex(input)
        self.SetRightLeadIndex(output)
        self.InitializeSelfEnergies()
        ScatteringGreenFunction.__init__(self, scatteringhamiltonian,
                                         internalscatteringhamiltonian)
        if Leads is None:
            # Test that the leads match the scattering region
            self.TestLeadsMatch(DeltaS)

    def SetLeadHamiltonians(self, Leads):
        self.Leads = Leads

    def GetNumberOfLeads(self):
        assert self.Leads is not None, "Please specify the lead Hamiltonians"
        return len(self.Leads)

    def GetLeadHamiltonian(self, lead):
        assert 0 <= lead and lead < self.GetNumberOfLeads(), "Lead index out of range"
        return self.Leads[lead]

    def GetLeftLeadHamiltonian(self):
        return self.GetLeadHamiltonian(self.GetLeftLeadIndex())

    def GetRightLeadHamiltonian(self):
        return self.GetLeadHamiltonian(self.GetRightLeadIndex())

    def GetLeadRepresentation(self, lead, matrix, dagger=False):
        HLead = self.GetLeadHamiltonian(lead)
        leadindices = HLead.GetLeadIndices()
        couplingindices = HLead.GetCouplingIndices()
        if dagger:
            xindices = leadindices
            yindices = couplingindices
        else:
            xindices = couplingindices
            yindices = leadindices
        n = len(xindices)
        m = len(yindices)
        t = np.zeros((xindices.len(), yindices.len()), np.complex)
        xslice = xindices.GetCompactSlices()
        yslice = yindices.GetCompactSlices()
        for i in range(n):
            for j in range(m):
                t[xslice[i], yslice[j]] = matrix[xindices[i], yindices[j]]
        return t

    def GetLeadInteractionRepresentation(self, lead):
        return self.GetLeadRepresentation(lead,
                                          matrix=self.GetScatteringHamiltonian().GetHamiltonianMatrix())

    def GetLeadInteractionRepresentationDagger(self, lead):
        return self.GetLeadRepresentation(lead,
                                          matrix=self.GetScatteringHamiltonian().GetHamiltonianMatrix(),
                                          dagger=True)

    def GetLeftLeadInteractionRepresentation(self):
        """Warning: The Method GetLeftLeadInteractionRepresentation is Deprecated"""
        return self.GetLeadInteractionRepresentation(self.GetLeftLeadIndex())

    def GetRightLeadInteractionRepresentation(self):
        """Warning: The Method GetRightLeadInteractionRepresentation is Deprecated"""
        return self.GetLeadInteractionRepresentation(self.GetRightLeadIndex())

    def GetLeadIdentityRepresentation(self, lead):
        return self.GetLeadRepresentation(lead,
                                          matrix=self.GetScatteringHamiltonian().GetOverlapMatrix())

    def GetLeadIdentityRepresentationDagger(self, lead):
        return self.GetLeadRepresentation(lead,
                                          matrix=self.GetScatteringHamiltonian().GetOverlapMatrix(),
                                          dagger=True)

    def SetLeftLeadIndex(self, input):
        self.input = input
        if (0 > input) | (input >= self.GetNumberOfLeads()):
            print("Left lead index out of range")

    def GetLeftLeadIndex(self):
        return self.input

    def SetRightLeadIndex(self, output):
        self.output = output
        if (0 > output) | (output >= self.GetNumberOfLeads()):
            print("Right lead index out of range")

    def GetRightLeadIndex(self):
        return self.output

    def GetRetardedEquation(self):
        # E*O - H - sigma1 - sigma2 - sigma3 - sigma4:
        H = self.GetComplexEnergy()*self.GetIdentityRepresentation()
        H = H - self.GetHamiltonianRepresentation()
        n = self.GetNumberOfLeads()
        for lead in range(n):
            leadselfenergy = self.GetLeadSelfEnergy(lead)
            couplingindices = self.GetLeadHamiltonian(lead).GetCouplingIndices().GetScatteringSlices()
            slices = couplingindices.GetCompactSlices()
            for i in range(len(couplingindices)):
                H[couplingindices[i], couplingindices[i]] -= leadselfenergy[slices[i], slices[i]]
        return H

    def SetLeftLeadHamiltonian(self, hl):
        return self.SetLeadHamiltonians(lead=self.GetLeftLeadIndex(), H=hl)

    def SetRightLeadHamiltonian(self, hl):
        return self.SetLeadHamiltonians(lead=self.GetRightLeadIndex(), H=hl)

    def GetLeftLeadGreenFunction(self):
        return self.GetLeadGreenFunction(lead=self.GetLeftLeadIndex())

    def GetRightLeadGreenFunction(self):
        return self.GetLeadGreenFunction(lead=self.GetRightLeadIndex())

    def GetLeadGreenFunction(self, lead):
        return self.greenfunctions[lead]

    def GetLeftLeadSelfEnergy(self):
        return self.GetLeadSelfEnergy(lead=self.GetLeftLeadIndex())

    def GetRightLeadSelfEnergy(self):
        return self.GetLeadSelfEnergy(lead=self.GetRightLeadIndex())

    def SetLeftLeadSelfEnergy(self, selfenergy):
        return self.SetLeadSelfEnergy(lead=self.GetLeftLeadIndex(), selfenergy=selfenergy)

    def SetRightLeadSelfEnergy(self, selfenergy):
        return self.SetLeadSelfEnergy(lead=self.GetRightLeadIndex(), selfenergy=selfenergy)

    def SetLeadSelfEnergy(self, lead, selfenergy):
        self.sigma[lead] = selfenergy

    def InitializeSelfEnergies(self):
        n = self.GetNumberOfLeads()
        self.sigma = []
        self.greenfunctions = []
        for i in range(n):
            self.sigma.append(None)
            self.greenfunctions.append(None)

    def GetLeadSelfEnergy(self, lead):
        if self.sigma[lead] is None:
            self.UpdateLeadSelfEnergy(lead)
        selfenergy = self.sigma[lead]
        if not self.ReferenceSelfEnergies():
            self.UnregisterLeadSelfEnergy(lead)
        return selfenergy

    def UnregisterLeadSelfEnergy(self, lead):
        # Try to unregister lead self energy
        if (not self.sigma[lead] is None) & (not hasattr(self, "widebandlimit")):
            self.sigma[lead] = None

    def UnregisterLeftLeadSelfEnergy(self):
        self.UnregisterLeadSelfEnergy(self.GetLeftLeadIndex())

    def UnregisterRightLeadSelfEnergy(self):
        self.UnregisterLeadSelfEnergy(self.GetRightLeadIndex())

    def SetLeftLeadGreenFunction(self, leadgreenfunction):
        self.SetLeadGreenFunction(leadgreenfunction, lead=self.GetLeftLeadIndex())

    def SetRightLeadGreenFunction(self, leadgreenfunction):
        self.SetLeadGreenFunction(leadgreenfunction, lead=self.GetRightLeadIndex())

    def SetLeadGreenFunction(self, lead, leadgreenfunction):
        #print "Green Functions: ", self.greenfunctions
        if (lead < 0) | (lead > self.GetNumberOfLeads()):
            print("Invalid lead index")
        leadgreenfunction.ReferenceGreenFunctionOff()
        self.greenfunctions[lead] = leadgreenfunction

    def InitializeLeadGreenFunctions(self):
        n = self.GetNumberOfLeads()
        for lead in range(n):
            H = self.GetLeadHamiltonian(lead).GetH()
            S = self.GetLeadHamiltonian(lead).GetS()
            H_intsc = self.GetLeadHamiltonian(lead).GetT()
            S_intsc = self.GetLeadHamiltonian(lead).GetInteractionS()
            self.SetLeadGreenFunction(lead=lead,
                                      leadgreenfunction=LeadGreenFunction(hamiltonian=H,
                                                                          interaction=H_intsc,
                                                                          identity=S,
                                                                          identityinteraction=S_intsc))

        # Initialize parent Green function
        H_sc = self.GetScatteringHamiltonian().GetH_s()
        S_sc = self.GetScatteringHamiltonian().GetS_s()
        GreenFunction.__init__(self, hamiltonian=H_sc, identity=S_sc)

    def UpdateLeftLeadSelfEnergy(self):
        self.UpdateLeadSelfEnergy(self.GetLeftLeadIndex())

    def UpdateRightLeadSelfEnergy(self):
        self.UpdateLeadSelfEnergy(self.GetRightLeadIndex())

    def UpdateLeadSelfEnergy(self, lead):
        # Calculating self energy due to the lead
        # Sigma = tau x g x tau'
        # First finding [g]^{-1}
        retardedeq = self.GetLeadGreenFunction(lead).GetRetardedEquation()
        if not hasattr(self, "internalscatter"):
            # Calculating: tau' =E*I' -H',
            tau = self.GetComplexEnergy()*self.GetLeadIdentityRepresentationDagger(lead)
            tau -= self.GetLeadInteractionRepresentationDagger(lead)
            # Using that: A = g x tau' <=>
            #             [g]^{-1} x A = tau'
            # [g]^{-1} is the retarded equation of left lead
            A = LinearAlgebra.solve(retardedeq, tau)
            del retardedeq
            tau = self.GetComplexEnergy()*self.GetLeadIdentityRepresentation(lead)
            tau -= self.GetLeadInteractionRepresentation(lead)

            sigma = np.dot(tau, A)
            del A
            del tau
        else:
            print("Warning: Internal Scattering not yet implemented for MultiLead Code")
            hi = self.GetLeadGreenFunction(lead).GetInteractionRepresentation()
            si = self.GetLeadGreenFunction(lead).GetInteractionIdentityRepresentation()
            # We assume that basis functions in the lead are orthonormal
            tau = self.GetComplexEnergy() * Dagger(si) - Dagger(hi)

            ##A=LinearAlgebra.solve(retardedeq, Dagger(hi))
            A = LinearAlgebra.solve(retardedeq, tau)
            tau = self.GetComplexEnergy() * si - hi
            sigma = np.dot(tau, A)

            # Here we get the energy from scattering Green function
            zS = self.GetComplexEnergy()*self.GetInternal_S(lead)
            retardedeq = zS-self.GetInternal_H(lead)
            l = sigma.shape[0]
            retardedeq1[:l, :l] = retardedeq[:l, :l]-sigma
            A = LinearAlgebra.solve(retardedeq, tau)
            del retardedeq
            tau = self.GetComplexEnergy() * self.GetInternal_S_intl() - \
                       self.GetInternal_H_intl()
            sigma = np.dot(tau, A)
        self.SetLeadSelfEnergy(lead, selfenergy=sigma)

    def TestLeadsMatch(self, DeltaS):
        """Test leads match with the scattering region"""
        testpass = True
        n = self.GetNumberOfLeads()
        H_S = self.GetScatteringHamiltonian().GetHamiltonianMatrix()
        S_S = self.GetScatteringHamiltonian().GetOverlapMatrix()
        for lead in range(n):
            HLead = self.GetLeadHamiltonian(lead)
            H_Ld = HLead.GetH()
            S_Ld = HLead.GetS()
            indices = HLead.GetCouplingIndices()
            if indices.len() != H_Ld.shape[0]:
                ##print "Unaligned coupling and lead indices"
                ##print indices.len(), H_Ld.shape[0]
                continue
            compact = indices.GetCompactSlices()
            m = len(indices)
            Hmax = 0
            Smax = 0
            for i in range(m):
                Hmax = max(Hmax,
                           max(abs(H_S[indices[i], indices[i]]
                                   -H_Ld[compact[i], compact[i]]).ravel()))
                Smax = max(Smax,
                           max(abs(S_S[indices[i], indices[i]]
                                   -S_Ld[compact[i], compact[i]]).ravel()))
            print("Lead "+str(lead)+" Hamiltonian Convergence  "+str(Hmax)+" eV")
            print("Lead "+str(lead)+" Overlap Convergence      "+str(Smax)+" eV")
            if Smax > DeltaS:
                print("Warning: Overlap Convergence for Lead "+str(lead)+" is above DeltaS="+str(DeltaS)+" eV")
                print("Warning: Lead "+str(lead)+" does not match Scattering Region")
                testpass = False
        return testpass

    def SetEnergy(self, energy):
        GreenFunction.SetEnergy(self, energy)
        # Propagating changes to lead green functions
        length = self.GetNumberOfLeads()
        for lead in range(length):
            try:
                self.GetLeadGreenFunction(lead).SetEnergy(energy)
            except AttributeError:
                print("Uh Oh!")
                pass
            self.UnregisterLeadSelfEnergy(lead)
