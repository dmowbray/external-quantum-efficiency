"""Tools for using the Transmission NEGF 4-terminal Program"""
#import Numeric
import copy
import numpy as np
#import FFT
from numpy import fft
from eqe.transport.ArrayTools_np import TranslateAlongAxis0
from eqe.transport.Hilbert_np import Hilbert
from eqe.transport.HamiltonianTools_np import RotateMatrix

def Dagger(matrix, mkcopy=1):
    """First change the axis: (Does not allocate a new array)"""
    matrix_conj = np.swapaxes(matrix, 0, 1)
    if mkcopy: # Allocate space for new array
        return np.conjugate(matrix_conj)
    np.multiply(matrix_conj.imag, -1, matrix_conj.imag)
    return matrix_conj

def LambdaFromSelfEnergy(selfenergy, copy_selfenergy=1):
    """Calculates lambda = i[sigma-sigma^dagger]"""
    if copy_selfenergy: # Allocate space for a new array
        lambda_lead = copy.copy(selfenergy)
    else:
        lambda_lead = selfenergy

    # Using that lambda = i [ (sigma.real-sigma.real^T)+
    #            i(sigma.imag+sigma.imag^T)]
    selfenergy_t = np.swapaxes(selfenergy, 0, 1)
    # np.array array performs a copy of the uncontiguous array
    np.subtract(lambda_lead.real, np.array(selfenergy_t.real), lambda_lead.real)
    np.add(lambda_lead.imag, np.array(selfenergy_t.imag), lambda_lead.imag)
    np.multiply(lambda_lead, complex(0, 1), lambda_lead)
    return lambda_lead

def FermiDistribution(energy, kBT=0.0):
    """Obtain Fermi-Dirac Distribution"""
    if kBT == 0.0:
        return 0.5*(1.0-np.sign(energy))
    value, sign = abs(energy/kBT), np.sign(energy)
    exponent = sign*min(value, 1.0e2)
    return 1.0/(1.0+np.exp(exponent))

#def WriteToNetCDFFile(filename, x, y):
#    from Scientific.IO.NetCDF import NetCDFFile
#    file = NetCDFFile(filename, 'w')
#    file.createDimension('Npoints', len(x))
#    myvara = file.createVariable('xvalues', np.float, ('Npoints', ))
#    myvara[:] = x
#    myvarb = file.createVariable('yvalues', np.float, ('Npoints', ))
#    myvarb[:] = y
#    file.sync()
#    file.close()

#def ReadFromNetCDFFile(filename):
#    from Scientific.IO.NetCDF import NetCDFFile
#    #from Dacapo import NetCDF
#    file = NetCDFFile(filename, 'r')
#    x = NetCDF.Entry(name='xvalues').ReadFromNetCDFFile(file).GetValue()
#    y = NetCDF.Entry(name='yvalues').ReadFromNetCDFFile(file).GetValue()
#    return x, y

def RotateArray(a, U):
    """Rotate Array"""
    b = np.zeros(a.shape, np.complex)
    for i, a_i in enumerate(a):
        b[i] = RotateMatrix(a_i, U)
    return b

def Convolution(f, g, zero_index, axis=0):
    """Calculates the convolution (f * g)(x) = (1/(2*pi))∫ dy f(y)g(x-y).
    This is the Fourier transform of f(t)g(t).
    If f and g are matrices the integral is performed along the specified axis.
    'zero_index' is the index number of the zero point.
    'time_translate' translates the two functions relative to each other by
    the specified number og grid points."""
    #import FFT
    #from numpy import fft
    f_fft = fft.fft(f, axis=axis)
    g_fft = fft.fft(g, axis=axis)
    product_fft = f_fft*g_fft
    pre = 1.0/(2*np.pi)
    return pre*TranslateAlongAxis0(fft.ifft(product_fft, axis=0), -zero_index)

def DirectConvolution(f, g, grid, de):
    """Calculates (1/pi)∫ dy f(y)g(x-y) directly (no FFT).
      'grid' is in integer points. 'de' is energy spacing."""
    s = [len(grid)]+list(f.shape[1:])
    h = np.zeros(s, np.complex)
    for i, grid_i in enumerate(grid):
        f_translate = TranslateAlongAxis0(f, -grid_i)
        h[i] = np.sum(f_translate*g)*de
    return h/np.pi

def AntiConvolution(f, g, zero_index, axis=0):
    """Calculates the anti-convolution (f ** g)(x) = (1/(2*pi))∫ dy f(x+y)g(y).
    This is the Fourier transform of f(t)g(-t).
    If f and g are matrices the integral is performed along the specified axis.
    'zero_index' is the index of the zero point."""
    #from numpy import fft
    f_fft = fft.fft(f, axis=axis)
    g_fft = np.conjugate(fft.fft(np.conjugate(g), axis=axis))
    product_fft = f_fft*g_fft
    pre = 1.0/(2*np.pi)
    return pre*TranslateAlongAxis0(fft.ifft(product_fft, axis=0),
                                   zero_index)

def HilbertTransform(f, energy):
    """Perform Hilbert Transform"""
    return Hilbert(f, nfft=len(energy), kerneltype='Simple')
