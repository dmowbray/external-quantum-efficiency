"""Hmiltonian Tools for 4-terminal NEGF Code"""
#import Numeric
import numpy as np
from numpy.linalg import eig, inv

def CutCoupling(h, indexlist):
    for index in indexlist:
        mask = np.ones(h.shape)
        mask[:, index] = np.zeros([h.shape[0]])
        mask[index, :] = np.zeros([h.shape[0]])
        mask[index, index] = 1
        h = h*mask
    return h

def GetCouplingToGroupOrbital(h, index):
    orb = h[:, index].copy()
    orb[index] = 0.0
    return np.sqrt(abs(np.sum(orb*np.conjugate(orb))))

def RotateMatrixFromCoordinateList(h, indexlst):
    M = np.identity(h.shape[0], np.complex)
    # Find involved orbitals
    newpos = []
    for i in range(len(indexlst)):
        for j in range(len(indexlst[i])):
            if not indexlst[i][j][0] in newpos:
                newpos.append(indexlst[i][j][0])
    print("Indices of new basis functions:", newpos)
    for i in range(len(indexlst)):
        vec = np.zeros([h.shape[0]], np.complex)
        for j in range(len(indexlst[i])):
            vec[indexlst[i][j][0]] = indexlst[i][j][1]
        # Normalize vec:
        vec = vec/(np.sqrt(np.sum(vec*np.conjugate(vec))))
        M[:, newpos[i]] = vec
    Md = np.transpose(np.conjugate(M))
    return np.dot(Md, np.dot(h, M)), M

def GetGroupOrbital(h, index, exclude=None):
    v = h[:, index].copy()
    v[index] = 0.0
    if exclude is not None:
        for i in exclude:
            v[i] = 0.0
    return v/np.sqrt(np.sum(v*np.conjugate(v)))

def RotateMatrix(h, U):
    """ U contains the new basis as its columns"""
    return np.dot(Dagger(U), np.dot(h, U))

def RotateMatrix2(h, listofstates, start):
    """ states listed in listofstates are kept, while an orthogonal basis
is chosen in the remaining subspace"""
    M = np.identity(h.shape[0], np.complex)
    order = range(h.shape[0])
    temp = range(start, start+len(listofstates))
    order[:len(listofstates)] = temp
    order[start:start+len(listofstates)] = range(len(listofstates))
    for i, state in enumerate(listofstates):
        assert state[i+start] != 0, ("ERROR: substitution of old state no."
                                     + str(start+i)
                                     +"by new state no."
                                     +str(i)+"is not possible since this will reduce dimensionality")
        M[:, i+start] = state
    Mnew = GramSchmidtOrthonormalize(M, order)
    Mnewd = np.transpose(np.conjugate(Mnew))
    return np.dot(Mnewd, np.dot(h, Mnew))

def Dagger(array):
    return np.transpose(np.conjugate(array))

def Project(a, b):
    """ returns the projection of b onto a"""
    return a*(np.dot(np.conjugate(a), b)/np.dot(np.conjugate(a), a))

def Normalize(vec):
    newvec = np.zeros(vec.shape, np.complex)
    M = vec.shape[1]
    for m in range(M):
        newvec[:, m] = vec[:, m]/np.sqrt(np.dot(np.conjugate(vec[:, m]), vec[:, m]))
    return newvec

def GramSchmidtOrthonormalize(vec, order=None):
    """ vec is a NxM matrix containing M vectors as its columns
    These will be orthogonalized by Gram-Schmidt using the order
    specified in the list 'order'"""
    newvec = np.zeros(vec.shape, np.complex)
    N, M = vec.shape[0], vec.shape[1]
    if order is None:
        order = range(M)
    for m in range(M):
        temp = vec[:, order[m]]
        for i in range(m):
            temp = temp-Project(newvec[:, order[i]], vec[:, order[m]])
        # Normalize before saving
        newvec[:, order[m]] = temp/np.sqrt(np.dot(np.conjugate(temp), temp))
    return newvec

def LowdinOrthonormalize(vec):
    """ vec is a NxM matrix containing M vectors as its columns.
        These will be orthogonalized by Lowdin procedure"""

    S = np.dot(Dagger(vec), vec)
    epsilon, U = eig(np.conjugate(S))

    # Now, U contains the eigenvectors as ROWS and epsilon the eigenvalues
    D = np.identity(S.shape[0], np.complex) / np.sqrt(epsilon)
    # T = S^(-1/2)
    T = np.dot(np.transpose(U),
               np.dot(D, np.conjugate(U)))
    return np.dot(vec, T)

def GetGreenFunction(hamiltonian, energy, eta):
    N = hamiltonian.shape[0]
    return inv((energy+1.0j*eta)*np.identity(N)-hamiltonian)

def GetSpectralFunction(listoforbitals, hamiltonian, listofenergies, width):
    """listoforbitals can contain indices,
    or normalized linear combinations
    width should be approximately given
    by the distance between eigenvalues:
    width = (2x) 10 eV/[no. of eigenvalues]"""
    N = hamiltonian.shape[0]
    Norb = len(listoforbitals)
    listofcoords = []
    for i in range(Norb):
        coords = np.zeros([N], np.complex)
        if isinstance(listoforbitals[i], int):
            coords[listoforbitals[i]] = 1.0
        else:
            coords = listoforbitals[i]
        listofcoords.append(coords)
    prefactor = -1.0/np.pi
    spectralfct = np.zeros([Norb, len(listofenergies)], np.float)
    mult = np.dot
    for i, en in enumerate(listofenergies):
        print(en)
        gf = GetGreenFunction(hamiltonian, en, width)
        for n, coords in enumerate(listofcoords):
            spectralfct[n, i] = abs(prefactor*
                                    (mult(mult(np.transpose(np.conjugate(coords)),
                                               gf),
                                          coords)).imag)
    return spectralfct
