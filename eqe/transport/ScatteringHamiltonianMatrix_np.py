"""Scattering Hamiltonian Matrix Class for Transmission Calculations"""

import copy

import numpy as np
import numpy.linalg as LinearAlgebra
from eqe.transport.ListOfSlices import ListOfSlices
from eqe.transport.LeadHamiltonianMatrix_np import LeftLeadHamiltonianMatrix, RightLeadHamiltonianMatrix


class ScatteringHamiltonianMatrix:
    """ Given the general Hamiltonian this class contains methods to slice out
        the relevant parts."""

    def __init__(self, leftprincipallayer, rightprincipallayer, hamiltonianmatrix=None, overlapmatrix=None, filename=None, leftlead=None, rightlead=None, scattering=None):
        """Initialization of Scattering Matrix"""
        if hamiltonianmatrix is not None:
            self.SetHamiltonianMatrix(hamiltonianmatrix)
        if overlapmatrix is not None:
            self.SetOverlapMatrix(overlapmatrix)
        if filename is not None:
            self.ReadHSMatrixFromNetCDFFile(filename)
        self.SetLeftPrincipalLayerLength(leftprincipallayer)
        self.SetRightPrincipalLayerLength(rightprincipallayer)
        self.SetIndices(leftlead, rightlead, scattering)


    def SetIndices(self, leftlead, rightlead, scattering):
        """Set indices for the left and right lead"""
        length = self.GetHamiltonianMatrix().shape[0]
        lprin = self.GetLeftPrincipalLayerLength()
        rprin = self.GetRightPrincipalLayerLength()
        if leftlead is None:
            leftlead = LeftLeadHamiltonianMatrix(lprin)
            leftlead.SetCouplingIndices(ListOfSlices([slice(lprin, length-rprin)]))
        leftlead.SetLength(length)
        self.SetLeftLead(leftlead)
        if rightlead is None:
            rightlead = RightLeadHamiltonianMatrix(rprin)
            rightlead.SetCouplingIndices(ListOfSlices([slice(lprin, length-rprin)]))
        rightlead.SetLength(length)
        self.SetRightLead(rightlead)
        if scattering is None:
            leadindices = ListOfSlices([])
            leadindices.extend(self.GetLeftLead().GetLeadIndices())
            leadindices.extend(self.GetRightLead().GetLeadIndices())
            scattering = leadindices.Difference(end=length)
        self.SetScatteringIndices(scattering)
        leftlead.SetScatteringSlices(scattering)
        rightlead.SetScatteringSlices(scattering)

    def GetScatteringIndices(self):
        """Return scattering Indices"""
        return self.scatteringindices

    def SetScatteringIndices(self, scatteringindices):
        """Set Scattering Indices"""
        self.scatteringindices = scatteringindices

    def SetLeftLead(self, leftlead):
        """Set Left Lead indices"""
        self.leftlead = leftlead

    def SetRightLead(self, rightlead):
        """Set Right Lead indices"""
        self.rightlead = rightlead

    def GetLeftLead(self):
        """Return Left Lead"""
        return self.leftlead

    def GetRightLead(self):
        """Return Right Lead"""
        return self.rightlead

    def GetScatteringIndices(self):
        """Return Scattering Indices"""
        return self.scatteringindices

    def GetRotationMatrix(self):
        """Return Rotation Matrix"""
        from LinearAlgebra import eigenvectors
        H = self.GetH_s()
        val, vec = eigenvectors(H)
        indices = np.argsort(val.real)
        val = np.take(val.real, indices)
        print("Eigenvalues:", val.real)
        vec = np.take(vec, indices)
        U = np.transpose(vec)
        return U

    def SetHamiltonianMatrix(self, hamiltonian):
        """Set Hamiltonian Matrix"""
        self.hmatrix = hamiltonian

    def GetHamiltonianMatrix(self):
        """Get Hamiltonian Matrix"""
        return self.hmatrix

    def SetOverlapMatrix(self, overlapmatrix):
        """Set Overlap Matrix"""
        """If not specified the overlapmatrix is chosen to be the
        identitymatrix."""
        self.omatrix = overlapmatrix

    def GetOverlapMatrix(self):
        """Return Overlap Matrix
        If not specified the overlapmatrix is chosen to be the
        Identity Matrix."""
        if not hasattr(self, 'omatrix'):
            return np.identity(self.GetHamiltonianMatrix().shape[0], np.complex)
        return self.omatrix

    def SetHartreeMatrix(self, hartree):
        """Set Hartree Matrix"""
        self.hartree = hartree

    def GetHartreeMatrix(self):
        """Return Hartree Matrix"""
        return self.hartree

    def SetXCMatrix(self, xc):
        """Set Exchange and Correlation Matrix"""
        self.xc = xc

    def GetXCMatrix(self):
        """Return Exchange and Correlation Matrix"""
        return self.xc

    def SetLeftPrincipalLayerLength(self, principallayer):
        """Set Length of Left Principal Layer"""
        self.leftprincipallayer = principallayer

    def GetLeftPrincipalLayerLength(self):
        """Return Length of Left Principal Layer"""
        if not hasattr(self, 'leftprincipallayer'):
            print("Please specify the length of a left principallayer.")
        else:
            return self.leftprincipallayer

    def SetRightPrincipalLayerLength(self, principallayer):
        """Set Right Principal Layer Length"""
        self.rightprincipallayer = principallayer

    def GetRightPrincipalLayerLength(self):
        """Return Right Principal Layer Length"""
        if not hasattr(self, 'rightprincipallayer'):
            print("Please specify the length of a right principallayer.")
        else:
            return self.rightprincipallayer

    def ReadHSMatrixFromNetCDFFile(self, filename):
        """ As an alternative it is possible to specify the
         Scatteringhamiltonian directly from the netCDF file given by the
         code. Here the Hamiltonian is found from the dacapocalculation
         and the following calculation of the Wannierfunctions -
         this matrix is called the HSMatrix.
         In the scatteringarea the Hamiltonen is given as;
         {H_l  t_l^d    0}
         {t_l   H_s   t_r}
         {0    t_r^d  H_r} , where d = dagger"""
        file = NetCDFFile(filename, 'r')
        HSMatrix = NetCDF.Entry(name='HSMatrix').ReadFromNetCDFFile(file).GetValue()
        hmat = np.zeros([HSMatrix.shape[1], HSMatrix.shape[1]], np.complex)
        smat = np.zeros([HSMatrix.shape[1], HSMatrix.shape[1]], np.complex)
        hmat.real = HSMatrix[0, :, :, 0]   # The two outermost parameters tells
        hmat.imag = HSMatrix[0, :, :, 1]   # whether it is the Hamiltonian, 0,
        smat.real = HSMatrix[1, :, :, 0]   # or the overlapsmatrix,
        smat.imag = HSMatrix[1, :, :, 1]   # 1, and whether the real, 0,
        self.SetHamiltonianMatrix(hmat)
        self.SetOverlapMatrix(smat)
        if HSMatrix.shape[0] == 4:
            hartree = np.zeros([HSMatrix.shape[1], HSMatrix.shape[1]], np.complex)
            xc = np.zeros([HSMatrix.shape[1], HSMatrix.shape[1]], np.complex)
            hartree.real = HSMatrix[2, :, :, 0]   # The two outermost parameters tells
            hartree.imag = HSMatrix[2, :, :, 1]   # whether it is the Hamiltonen, 0,
            xc.real = HSMatrix[3, :, :, 0]   # or the overlapsmatrix,
            xc.imag = HSMatrix[3, :, :, 1]
            self.SetHartreeMatrix(hartree)  # or imaginaere part, 1, is given.
            self.SetXCMatrix(xc)

    def WriteHSMatrixToNetCDFFile(self, filename):
        """Write Hamiltonian and Overlap Matrices to a NetCDF File"""
        hmat = self.GetHamiltonianMatrix()
        smat = self.GetOverlapMatrix()

        file = NetCDFFile(filename, 'w')
        file.createDimension("Complex", 2)
        file.createDimension("NObjects", 2)
        file.createDimension("NBasisFunctions", hmat.shape[0])

        HSMatrix = file.createVariable("HSMatrix", 'd', ("NObjects", "NBasisFunctions", "NBasisFunctions", "Complex"))
        HSMatrix[0, :, :, 0] = hmat.real
        HSMatrix[0, :, :, 1] = hmat.imag
        HSMatrix[1, :, :, 0] = smat.real
        HSMatrix[1, :, :, 1] = smat.imag
        file.sync()
        file.close()


    def ReduceCoupling(self, factor):
        """Reduce Coupling"""
        coupling = self.GetLeftLead().GetCouplingIndices()
        lead = self.GetLeftLead().GetLeadIndices()
        for i in range(len(coupling)):
            for j in range(len(lead)):
                self.GetHamiltonianMatrix()[coupling[i], lead[j]] = factor*self.GetT_l()[coupling.compact[i], lead.compact[j]]
                self.GetOverlapMatrix()[coupling[i], lead[j]] = factor*self.GetInteractionS_l()[coupling.compact[i], lead.compact[j]]
        coupling = self.GetRightLead().GetCouplingIndices()
        lead = self.GetRightLead().GetLeadIndices()
        for i in range(len(coupling)):
            for j in range(len(lead)):
                self.GetHamiltonianMatrix()[coupling[i], lead[j]] = factor*self.GetT_r()[coupling.compact[i], lead.compact[j]]
                self.GetOverlapMatrix()[coupling[i], lead[j]] = factor*self.GetInteractionS_r()[coupling.compact[i], lead.compact[j]]

    # Methods with which it is possible to define and call the Hamiltonian
    # for the scatteringarea.
    def SetH_s(self, Hs):
        """Set Scattering Matrix"""
        indices = self.GetScatteringIndices()
        H = self.GetHamiltonianMatrix()
        for i, j in range(len(indices)):
            H[indices[i], indices[j]] = Hs[indices.compact[i], indices.compact[j]]
        self.SetHamiltonianMatrix(H)

    def GetRepresentation(self, matrix, indices):
        """Return Representation Method for Square Matrices"""
        return self.GetRectRepresentation(matrix, xindices=indices, yindices=indices)

    def GetH_s(self):
        """Return Scattering Hamiltonian Matrix"""
        return self.GetRepresentation(self.GetHamiltonianMatrix(),
                                      self.GetScatteringIndices())

    def GetXC_s(self):
        """Return Exchange and Correlation Matrix"""
        return self.GetRepresentation(self.GetXCMatrix(),
                                      self.GetScatteringIndices())

    def GetS_s(self):
        """Return Overlap Matrix"""
        return self.GetRepresentation(self.GetOverlapMatrix(),
                                      self.GetScatteringIndices())

    # Often one wants to make sure that the supercell for the scattering area
    # is chosen big enough so that the effective potential in the leads is
    # converged to the value in bulk.
    # For that reason it is also possible to call the two leadhamiltonians
    # defined in the dacapocalculation of the scatteringregion.


    def GetH_l(self):
        """Return Left Lead Hamiltonian"""
        return self.GetRepresentation(self.GetHamiltonianMatrix(),
                                      self.GetLeftLead().GetLeadIndices())


    def GetS_l(self):
        """Return Left Lead Overlap Matrix"""
        return self.GetRepresentation(self.GetOverlapMatrix(),
                                      self.GetLeftLead().GetLeadIndices())


    def GetH_r(self):
        """Return Right Lead Hamiltonian Matrix"""
        return self.GetRepresentation(self.GetHamiltonianMatrix(),
                                      self.GetRightLead().GetLeadIndices())


    def GetS_r(self):
        """Return Right Lead Overlap Matrix"""
        return self.GetRepresentation(self.GetOverlapMatrix(),
                                      self.GetRightLead().GetLeadIndices())

    # Methods used for defining and calling the interactionhamiltonian
    # between the scatteringmolecule and the two leads.

    def GetRectRepresentation(self, matrix, xindices, yindices):
        """Return Rectangular Representation of a matrix"""
        xlength = xindices.len()
        ylength = yindices.len()
        matrix_s = np.zeros((xlength, ylength), np.complex)
        for i in range(len(xindices)):
            for j in range(len(yindices)):
                matrix_s[xindices.compact[i], yindices.compact[j]] = matrix[xindices[i], yindices[j]]
        return matrix_s

    def GetT_l(self):
        """Return Transpose of Left Lead Hamiltonian"""
        return self.GetRectRepresentation(self.GetHamiltonianMatrix(), self.GetLeftLead().GetCouplingIndices(), self.GetLeftLead().GetLeadIndices())


    def GetInteractionS_l(self):
        """Return Left Lead Overlap Matrix"""
        return self.GetRectRepresentation(self.GetOverlapMatrix(), self.GetLeftLead().GetCouplingIndices(), self.GetLeftLead().GetLeadIndices())


    def GetT_r(self):
        """Return Right Lead Transpose Matrixx"""
        return self.GetRectRepresentation(self.GetHamiltonianMatrix(), self.GetRightLead().GetCouplingIndices(), self.GetRightLead().GetLeadIndices())


    def GetInteractionS_r(self):
        """Return Right Lead Overlap Matrix"""
        return self.GetRectRepresentation(self.GetOverlapMatrix(), self.GetRightLead().GetCouplingIndices(), self.GetRightLead().GetLeadIndices())
