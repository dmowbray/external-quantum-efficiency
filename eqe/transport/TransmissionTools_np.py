"""Transmission Tools for NEGF 4-terminal Method"""

import numpy as np
import numpy.linalg as LinearAlgebra
from eqe.transport import GreenFunctions_np as GreenFunctions
from eqe.transport.Tools_np import Dagger, LambdaFromSelfEnergy
#from Scientific.IO.NetCDF import NetCDFFile
#from Dacapo import NetCDF
#import Numeric

class TransmissionTool:
    """Basis class for TransmissionTool"""
    # The method needs three hamiltonians as input, typically the user will
    # initialize these by using the hamiltonianclasses.

    def __init__(self,
                 scatteringhamiltonian=None,
                 leftleadhamiltonian=None,
                 rightleadhamiltonian=None,
                 energies=None,
                 prinlength=None,
                 internalscatteringhamiltonian=None,
                 neigenchannels=None,
                 scatteringgreenfunction=None):
        if scatteringgreenfunction is not None:
            self.SetScatteringGreenFunction(scatteringgreenfunction)
        if energies is not None:
            self.SetEnergies(energies)
        if prinlength is not None:
            self.SetPrincipalLayerLength(prinlength)
        if neigenchannels is not None:
            self.SetNumberOfEigenChannels(neigenchannels)
        if scatteringhamiltonian is not None:
            self.SetScatteringHamiltonian(scatteringhamiltonian)
        if internalscatteringhamiltonian is not None:
            self.SetInternalScatteringHamiltonian(internalscatteringhamiltonian)
        if leftleadhamiltonian is not None:
            self.SetLeftLeadHamiltonian(leftleadhamiltonian)
        if rightleadhamiltonian is not None:
            self.SetRightLeadHamiltonian(rightleadhamiltonian)
        #Align the Fermi level of the scattering region to the lead Fermi level
        self.AlignFermiLevels()
        ##print "Principal layer convergence: " + str(self.GetPrincipalLayerConvergence()) + " eV\n"

    def InitScatteringGreenFunction(self):
        # This method assumes that scattering and lead Hamiltonians exists
        H_sc = self.GetScatteringHamiltonian()
        H_l = self.GetLeftLeadHamiltonian()
        H_r = self.GetRightLeadHamiltonian()
        self.SetScatteringGreenFunction(
            GreenFunctions.ScatteringGreenFunction(
                scatteringhamiltonian=H_sc,
                leftleadhamiltonian=H_l,
                rightleadhamiltonian=H_r))
        if hasattr(self, 'inthsmatrix'):
            H_int = self.GetInternalScatteringHamiltonian()
            self.GetScatteringGreenFunction().SetInternalScatteringHamiltonian(H_int)

    def InitMultiLeadScatteringGreenFunction(self, Leads=[],
                                             left=0, right=1):
        # This method assumes that scattering and lead Hamiltonians exists
        if Leads == []:
            Leads = [self.GetLeftLeadHamiltonian(),
                     self.GetRightLeadHamiltonian()]
        H_sc = self.GetScatteringHamiltonian()
        self.SetScatteringGreenFunction(GreenFunctions.MultiLeadScatteringGreenFunction(
            scatteringhamiltonian=H_sc,
            Leads=Leads,
            input=left,
            output=right))
        if hasattr(self, 'inthsmatrix'):
            H_int = self.GetInternalScatteringHamiltonian()
            self.GetScatteringGreenFunction().SetInternalScatteringHamiltonian(H_int)

    def AlignFermiLevels(self, h00_l=None):
        """Aligns the fermi level of the scattering region
           to the fermi level of the lead, or to the argument e0.

           The aligning is done by matching a matrix element
           <w|H|w> (the first) in the principal layer
           in the scattering region to the corresponding matrix element
           in the lead"""

        #instances
        Hs = self.GetScatteringHamiltonian()
        Hl = self.GetLeftLeadHamiltonian()#assuming the leads are identical
        #matrices
        h00_s = Hs.GetH_l()[0, 0]
        if h00_l is None:
            h00_l = Hl.GetH_l()[0, 0]
        S = Hs.GetOverlapMatrix()
        H = Hs.GetHamiltonianMatrix()
        #do the alignment
        Hs_aligned = H-(h00_s-h00_l)*S
        #update the matrix of the Hs instance
        Hs.SetHamiltonianMatrix(Hs_aligned)
        if hasattr(self, 'inthsmatrix'):
            Hs_int = self.GetInternalScatteringHamiltonian()
            Hs_int.SetHamiltonianMatrix(Hs_aligned.copy())

    def GetPrincipalLayerConvergence(self, pl1=None, pl2=None):
        """The function returns a real number, which is the
           absolute value of the largest difference of the
           matrix elements of the principal layer in the lead
           and in the scattering region.
           These should be as small as possible
           (say below at least 0.2 eV)."""
        #instances
        Hs = self.GetScatteringHamiltonian()
        Hl = self.GetLeftLeadHamiltonian()#assuming the leads are identical

        #matrices
        if pl1 is None or pl2 is None:
            #no principal layers was specified ->
            #use the lead and S region p.l.
            pl1 = Hs.GetH_l() # leftmost principallayer in the S region
            pl2 = Hl.GetH_l() # principallayer in the lead

        err = max(np.absolute((pl2-pl1).ravel()))
        return err

    def SetScatteringHamiltonian(self, hs):
        self.hsmatrix = hs

    def GetScatteringHamiltonian(self):
        return self.hsmatrix

    def SetInternalScatteringHamiltonian(self, hs):
        self.inthsmatrix = hs

    def GetInternalScatteringHamiltonian(self):
        return self.inthsmatrix

    def SetLeftLeadHamiltonian(self, hl):
        self.hlmatrix = hl

    def GetLeftLeadHamiltonian(self):
        assert hasattr(self, 'hlmatrix'), "Please specify the leftleadhamiltonian."
        return self.hlmatrix

    def SetRightLeadHamiltonian(self, hr):
        self.hrmatrix = hr

    def GetRightLeadHamiltonian(self):
        assert hasattr(self, 'hrmatrix'), "Please specify the rightleadhamiltonian."
        return self.hrmatrix

    def SetEnergies(self, energies):
        self.energies = energies

    def GetEnergies(self):
        return self.energies

    def SetPrincipalLayerLength(self, prinlength):
        """Number of basis functions in one pricipal layer"""
        self.prinlength = prinlength

    def GetPrincipalLayerLength(self):
        return self.prinlength


    def GetNumberOfBasisFunctions(self):
        """Total number of basis functions in the calculation
        of the central region"""
        return self.GetScatteringHamiltonian().shape[0]

    def GetNumberOfScatteringBasisFunctions(self):
        """Number of basis functions in scattering region"""
        if hasattr(self, 'inthsmatrix'):
            return self.GetInternalScatteringHamiltonian().GetH_s().shape[0]
        return self.GetScatteringHamiltonian().GetH_s().shape[0]

    def SetNumberOfEigenChannels(self, neigenchannels):
        self.neigenchannels = neigenchannels

    def GetNumberOfEigenChannels(self):
        if not hasattr(self, 'neigenchannels'):
            # default set to 5
            self.neigenchannels = min(5, self.GetNumberOfScatteringBasisFunctions())
        return self.neigenchannels

    def SetScatteringGreenFunction(self, scatteringgreenfunction):
        self.scattergf = scatteringgreenfunction

    def GetScatteringGreenFunction(self):
        return self.scattergf

    def GetLeftLeadGreenFunction(self):
        return self.GetScatteringGreenFunction().GetLeftLeadGreenFunction()

    def GetRightLeadGreenFunction(self):
        return self.GetScatteringGreenFunction().GetRightLeadGreenFunction()

    def UpdateDOS(self):
        self.dos = 1

    def UpdateSpectralFunctions(self, listofextraorbitals=None):
        self.spectralfunctions = 1
        if listofextraorbitals is not None:
            self.SetListOfExtraOrbitals(listofextraorbitals)

    def UpdateTransmission(self):
        self.transmission = 1

    def UpdateEigenChannels(self, nchannels=None):
        self.eigenchannels = 1
        if nchannels is not None:
            self.SetNumberOfEigenChannels(nchannels)


    def GetTransmissionFunction(self):
        scatteringgf = self.GetScatteringGreenFunction()
        # Using: t = Tr[G^{r}xLambda^{L}xG^{a}xLambda^{R}]
        GF = scatteringgf.GetRepresentation()
        #X = ArrayTools.MatrixMultiplication(GF, LambdaFromSelfEnergy(scatteringgf.GetLeftLeadSelfEnergy(), copy_selfenergy = 1))
        ##X = np.matrixmultiply(GF, LambdaFromSelfEnergy(scatteringgf.GetLeadSelfEnergy(lead = left), copy_selfenergy = 1))
        sigma = scatteringgf.GetLeftLeadSelfEnergy()
        ## Initialize the interaction region of G as smG
        #scatslices = scatteringgf.GetLeftLeadHamiltonian().GetCouplingIndices().GetScatteringSlices()
        #compact = scatslices.GetCompactSlices()
        #leftlength = scatslices.len()
        #smG = np.zeros((leftlength, leftlength), np.complex)
        #for i in range(len(scatslices)):
        #    smG[compact[i], compact[i]] = G[scatslices[i], scatslices[i]]
        X = np.dot(GF,
                   LambdaFromSelfEnergy(sigma,
                                        copy_selfenergy=1))
        #Y = ArrayTools.MatrixMultiplication(Dagger(GF), LambdaFromSelfEnergy(scatteringgf.GetRightLeadSelfEnergy(), copy_selfenergy = 1))
        ##Y = np.dot(Dagger(GF), LambdaFromSelfEnergy(scatteringgf.GetLeadSelfEnergy(lead = right), copy_selfenergy = 1))

        sigma = scatteringgf.GetRightLeadSelfEnergy()
        GF = Dagger(GF)
        Y = np.dot(GF,
                   LambdaFromSelfEnergy(sigma,
                                        copy_selfenergy=1))
        #T = ArrayTools.MatrixMultiplication(X, Y)
        T = np.dot(X, Y)
        return T

    def GetTransmissionCoefficientFromTransmissionFunction(self):
        if not hasattr(self, 'transmissionfunction'):
            T = self.GetTransmissionFunction(energy)
        else:
            T = self.transmissionfunction
            delattr(self, "transmissionfunction")
        return abs(np.sum(np.diagonal(T)))

    def GetTransmissionCoefficient(self):
        scatteringgf = self.GetScatteringGreenFunction()
        # Using: t = Tr[G^{r}xLambda^{L}xG^{a}xLambda^{R}]
        GF = scatteringgf.GetRepresentation()
        #X = ArrayTools.MatrixMultiplication(GF, LambdaFromSelfEnergy(scatteringgf.GetLeftLeadSelfEnergy(), copy_selfenergy = 1))
        X = np.dot(GF, LambdaFromSelfEnergy(scatteringgf.GetLeftLeadSelfEnergy(),
                                            copy_selfenergy=1))
        #Y = ArrayTools.MatrixMultiplication(Dagger(GF), LambdaFromSelfEnergy(scatteringgf.GetRightLeadSelfEnergy(), copy_selfenergy = 1))
        Y = np.dot(Dagger(GF),
                   LambdaFromSelfEnergy(
                       scatteringgf.GetRightLeadSelfEnergy(),
                       copy_selfenergy=1))
        #Y = ArrayTools.Transpose_SquareMatrix(Y)
        Y = np.array(np.transpose(Y))
        t = np.dot(X.ravel(), Y.ravel())
        return t

    def GetEigenChannelTransmission(self, nchannels=None):
        if nchannels is not None:
            self.SetNumberOfEigenChannels(nchannels)
        T = self.GetTransmissionFunction()
        self.transmissionfunction = T
        # print "Diagonalizing T"
        t = np.sort(LinearAlgebra.eigvals(T).real)[-self.GetNumberOfEigenChannels():]
        return t

    def GetDensityOfStates(self):
        scatteringgf = self.GetScatteringGreenFunction()
        # Using: dos = -1/pi * Tr(X), where
        # X = GxO = (E*I-H)^{-1}xO
        G = scatteringgf.GetRepresentation()
        O = scatteringgf.GetIdentityRepresentation()
        #GO = ArrayTools.MatrixMultiplication(G, O)
        GO = np.dot(G, O)
        prefactor = -1.0/np.pi
        dos = prefactor*np.trace(GO.imag)
        return dos

    def GetSpectralFunctions(self):
        """ Returns a list of spectral functions of the basis functions"""
        scatteringgf = self.GetScatteringGreenFunction()
        O = scatteringgf.GetIdentityRepresentation()
        G = scatteringgf.GetRepresentation()
        # A_nn = Imag[OGO]_ii x (O_ii)^{-1}
        #GO = ArrayTools.MatrixMultiplication(G, O)
        GO = np.dot(G, O)
        #OGO = ArrayTools.MatrixMultiplication(O, GO)
        OGO = np.dot(O, GO)
        prefactor = -1.0/np.pi
        spectral = np.zeros([self.GetNumberOfScatteringBasisFunctions()+self.GetNumberOfExtraOrbitals()], np.float)
        spectral1 = (np.diagonal(OGO)*(1.0/np.diagonal(O))).imag
        spectral1 = abs(prefactor*spectral1)
        spectral[:self.GetNumberOfScatteringBasisFunctions()] = spectral1
        if self.GetNumberOfExtraOrbitals() > 0:
            # Using that
            extra = self.GetListOfExtraOrbitals()
            for i in range(self.GetNumberOfExtraOrbitals()):
                nominator = np.dot(Dagger(extra[i]), np.dot(OGO, extra[i]))
                denominator = np.dot(Dagger(extra[i]), np.dot(O, extra[i]))
                spectral[self.GetNumberOfScatteringBasisFunctions()+i] = abs(prefactor*(nominator/denominator).imag)
        return spectral

    def SetListOfExtraOrbitals(self, extraorb):
        self.extraorb = []
        for element in extraorb:
            element = np.array(element)
            extendedelement = np.zeros([self.GetNumberOfScatteringBasisFunctions()], np.complex)
            for i in range(element.shape[0]):
                extendedelement[int(element[i, 0])] = element[i, 1]
            self.extraorb.append(extendedelement)

    def GetListOfExtraOrbitals(self):
        return self.extraorb

    def GetNumberOfExtraOrbitals(self):
        if not hasattr(self, 'extraorb'):
            return 0
        return len(self.extraorb)

    def InitNetCDFFile(self, filename):
        """Writing important data to netcdf file"""
        energies = self.GetEnergies()
        # Initialize netcdf file for output:
        file = NetCDFFile(filename, 'w')
        file.createDimension("NEnergies", len(energies))
        file.createDimension("Complex", 2)
        file.createDimension("Single", 1)
        file.createDimension("NEigenChannels", self.GetNumberOfEigenChannels())
        file.createDimension("NScatteringBasisFunctions", self.GetNumberOfScatteringBasisFunctions())
        file.createDimension("NSpectralFunctions", self.GetNumberOfScatteringBasisFunctions()+self.GetNumberOfExtraOrbitals())
        # Creating variable for energy
        energies_file = file.createVariable("Energies", np.asarray(energies).typecode(), ("NEnergies", ))
        energies_file[:] = self.GetEnergies()
        # Write data and return file
        file.sync()
        return file

    def UpdateToNetCDFFile(self, filename):
        file = self.InitNetCDFFile(filename)
        energies = self.GetEnergies()

        # Create variables for
        if hasattr(self, 'transmission'):
            t_file = file.createVariable("Transmission", 'd', ("NEnergies", ))
            t_file[:] = np.zeros([len(energies)], np.float)
            file.sync()
        if hasattr(self, 'dos'):
            dos_file = file.createVariable("DensityOfStates", 'd', ("NEnergies", ))
        if hasattr(self, 'eigenchannels'):
            eigen_file_lst = []
            for j in range(self.GetNumberOfEigenChannels()):
                name = "EigenChannel_"+str(j)
                eigen_file_lst.append(file.createVariable(name, 'd', ("NEnergies", )))
        if hasattr(self, 'spectralfunctions'):
            Nspec = self.GetNumberOfScatteringBasisFunctions()+self.GetNumberOfExtraOrbitals()
            spec_file_lst = []
            for j in range(Nspec):
                name = "SpectralFunction_"+str(j)
                spec_file_lst.append(file.createVariable(name, 'd', ("NEnergies", )))

        # Make sure that representations are kept until deleted on purpose
        self.GetScatteringGreenFunction().ReferenceGreenFunctionOn()
        self.GetScatteringGreenFunction().ReferenceSelfEnergiesOn()
        # Loop over the energies:
        for i in range(len(energies)):
            #print energies[i]
            self.GetScatteringGreenFunction().SetEnergy(energies[i])
            if hasattr(self, 'eigenchannels'):
                chan = self.GetEigenChannelTransmission(self.GetNumberOfEigenChannels())
                for j in range(self.GetNumberOfEigenChannels()):
                    eigen_file_lst[j][i] = chan[j]
            if hasattr(self, 'transmission'):
                if not hasattr(self, 'eigenchannels'):
                    t = abs(self.GetTransmissionCoefficient())
                else:
                    t = self.GetTransmissionCoefficientFromTransmissionFunction()
                t_file[i] = t
            if hasattr(self, 'dos'):
                dos = self.GetDensityOfStates()
                dos_file[i] = dos
            if hasattr(self, 'spectralfunctions'):
                spectral = self.GetSpectralFunctions()
                for j in range(Nspec):
                    spec_file_lst[j][i] = spectral[j]
            # write the data
            file.sync()
            # Delete representations of GF and self energies
            self.GetScatteringGreenFunction().UnregisterRepresentation()
            self.GetScatteringGreenFunction().UnregisterLeftLeadSelfEnergy()
            self.GetScatteringGreenFunction().UnregisterRightLeadSelfEnergy()
        # finally close the file
        file.close()

    def UpdateToTextFile(self, filename):
        file = open(filename, 'a')
        energies = self.GetEnergies()

        # Make sure that representations are kept until deleted on purpose
        self.GetScatteringGreenFunction().ReferenceGreenFunctionOn()
        self.GetScatteringGreenFunction().ReferenceSelfEnergiesOn()
        # Loop over the energies:
        for i in range(len(energies)):
            #print energies[i]
            self.GetScatteringGreenFunction().SetEnergy(energies[i])
            file.write(str(energies[i])+' ')
            if hasattr(self, 'transmission'):
                if not hasattr(self, 'eigenchannels'):
                    t = abs(self.GetTransmissionCoefficient())
                else:
                    t = self.GetTransmissionCoefficientFromTransmissionFunction()
                file.write(str(t)+' ')
            if hasattr(self, 'dos'):
                dos = self.GetDensityOfStates()
                file.write(str(dos)+' ')
            if hasattr(self, 'spectralfunctions'):
                spectral = self.GetSpectralFunctions()
                for j in range(Nspec):
                    file.write(str(spectral[j])+' ')
            if hasattr(self, 'eigenchannels'):
                chan = self.GetEigenChannelTransmission(self.GetNumberOfEigenChannels())
                for j in range(self.GetNumberOfEigenChannels()):
                    file.write(str(chan[j])+' ')
            
            # write the data
            file.write('\n')
            file.flush()
            # Delete representations of GF and self energies
            self.GetScatteringGreenFunction().UnregisterRepresentation()
            self.GetScatteringGreenFunction().UnregisterLeftLeadSelfEnergy()
            self.GetScatteringGreenFunction().UnregisterRightLeadSelfEnergy()
        # finally close the file
        file.close()
