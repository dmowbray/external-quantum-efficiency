"""Lead Hamiltonians for 4-terminal NEGF Code"""
import numpy as np
from eqe.transport.ListOfSlices import ListOfSlices

class LeadHamiltonianMatrix:
    """ Given the bulkhamiltonian this class returns the hamiltonian and
        overlapmatrix for the leads."""
    def __init__(self,
                 principallayer,
                 hamiltonianmatrix=None,
                 overlapmatrix=None,
                 filename=None,
                 leadindices=None,
                 couplingindices=None,
                 leadtype=None):
        if hamiltonianmatrix is not None:
            self.SetHamiltonianMatrix(hamiltonianmatrix)
        if overlapmatrix is not None:
            self.SetOverlapMatrix(overlapmatrix)
        if filename is not None:
            self.ReadHSMatrixFromNetCDFFile(filename)
        self.SetPrincipalLayerLength(principallayer)
        self.SetLeadType(leadtype)
        self.SetLeadAndCouplingIndices(leadindices, couplingindices)

    def SetLength(self, length):
        self.GetLeadIndices().WrapNegativeSlices(end=length)

    def SetLeadType(self, leadtype):
        if (leadtype == "input") | (leadtype == "left"):
            self.isinput = True
        elif (leadtype == "output") | (leadtype == "right"):
            self.isinput = False
        else:
            print("Set lead type as input/left or output/right")

    def IsInput(self):
        return self.isinput

    def IsOutput(self):
        return not self.isinput

    def SetHamiltonianMatrix(self, hamiltonian):
        self.hmatrix = hamiltonian

    def GetHamiltonianMatrix(self):
        return self.hmatrix

    def SetOverlapMatrix(self, overlapmatrix):
        """If not specified the overlapmatrix is given by the
           identitymatrix."""
        self.omatrix = overlapmatrix

    def GetOverlapMatrix(self):
        if not hasattr(self, 'omatrix'):
            return np.identity(self.GetHamiltonianMatrix().shape[0], np.complex)
        return self.omatrix

    def SetPrincipalLayerLength(self, principallayer):
        self.principallayer = principallayer

    def SetLeadAndCouplingIndices(self, leadindices, couplingindices):
        principallayer = self.GetPrincipalLayerLength()

        if leadindices is None:
            # If leadindices is unspecified, use input/left or output/right
            # to designate the leadindices
            if self.IsInput():
                #print "Setting Lead & Coupling Indices for input/left lead"
                self.SetLeadIndices(ListOfSlices([slice(0, principallayer)]))
            elif self.IsOutput():
                #print "Setting Lead & Coupling Indices for output/right lead"
                self.SetLeadIndices(ListOfSlices([slice(-principallayer, 0)]))
        else:
            if isinstance(leadindices, int):
                # If leadindices is simply an index, assume it is a start position
                self.SetLeadIndices(ListOfSlices([slice(leadindices,
                                                        leadindices+principallayer)]))
            else:
                self.SetLeadIndices(leadindices)
        if couplingindices is not None:
            if isinstance(couplingindices, int):
                self.SetCouplingIndices(ListOfSlices([slice(couplingindices,
                                                            couplingindices+principallayer)]))
            else:
                self.SetCouplingIndices(couplingindices)
        else:
            couplingindices = ListOfSlices([])
            leadindices = self.GetLeadIndices()
            for leadslice in leadindices:
                shift = (self.IsInput()-self.IsOutput())*principallayer
                couplingindices.append(slice(leadslice.start+shift,
                                             leadslice.stop+shift,
                                             leadslice.step))
            self.SetCouplingIndices(couplingindices)

    def SetLeadIndices(self, leadindices):
        """Setting Lead Indices:
        leadindices"""
        self.leadindices = leadindices

    def SetCouplingIndices(self, couplingindices):
        """Setting Coupling Indices:
        couplingindices"""
        self.couplingindices = couplingindices

    def GetPrincipalLayerLength(self):
        assert hasattr(self, 'principallayer'), "Please specify the length of a principallayer."
        return self.principallayer

    def GetLeadIndices(self):
        assert hasattr(self, 'leadindices'), "Please specify the lead indices."
        return self.leadindices

    def GetCouplingIndices(self):
        """Coupling Indices Requested
        self.couplingindices"""
        assert hasattr(self, 'couplingindices'), "Please specify the coupling indices."
        return self.couplingindices

    def ReadHSMatrixFromNetCDFFile(self, filename):
        """ As an alternative it is  possible to read in the leadhamiltonian
            from an netCDF file given as the output from an dacapocalculation.
            In bulk the Hamiltonen is given as;
                {h  t^d} , where d = dagger
            H = {t    h}"""

        file = NetCDFFile(filename, 'r')
        HSMatrix = NetCDF.Entry(name='HSMatrix').ReadFromNetCDFFile(file).GetValue()
        hmat = np.zeros([HSMatrix.shape[1], HSMatrix.shape[1]], np.complex)
        smat = np.zeros([HSMatrix.shape[1], HSMatrix.shape[1]], np.complex)
        hmat.real = HSMatrix[0, :, :, 0]
        hmat.imag = HSMatrix[0, :, :, 1]
        smat.real = HSMatrix[1, :, :, 0]
        smat.imag = HSMatrix[1, :, :, 1]
        self.SetHamiltonianMatrix(hmat)
        self.SetOverlapMatrix(smat)

    def WriteHSMatrixToNetCDFFile(self, filename):
        hmat = self.GetHamiltonianMatrix()
        smat = self.GetOverlapMatrix()

        file = NetCDFFile(filename, 'w')
        file.createDimension("Complex", 2)
        file.createDimension("NObjects", 2)
        file.createDimension("NBasisFunctions", hmat.shape[0])

        HSMatrix = file.createVariable("HSMatrix",
                                       'd',
                                       ("NObjects",
                                        "NBasisFunctions",
                                        "NBasisFunctions",
                                        "Complex"))
        HSMatrix[0, :, :, 0] = hmat.real
        HSMatrix[0, :, :, 1] = hmat.imag
        HSMatrix[1, :, :, 0] = smat.real
        HSMatrix[1, :, :, 1] = smat.imag
        file.sync()
        file.close()

    def GetH(self):
        """Returns the interaction Hamiltonian matrix H"""
        prinlength = self.GetPrincipalLayerLength()
        return self.GetHamiltonianMatrix()[0:prinlength, 0:prinlength]

    def GetS(self):
        """Returns the overlap matrix S"""
        prinlength = self.GetPrincipalLayerLength()
        return self.GetOverlapMatrix()[0:prinlength, 0:prinlength]

    def GetT(self):
        """Returns the interaction matrix T"""
        prinlength = self.GetPrincipalLayerLength()
        xpos = self.IsInput()*prinlength
        ypos = self.IsOutput()*prinlength
        return self.GetHamiltonianMatrix()[xpos:xpos+prinlength, ypos:ypos+prinlength]

    def GetInteractionS(self):
        """Returns the overlap interaction matrix S"""
        prinlength = self.GetPrincipalLayerLength()
        xpos = self.IsInput()*prinlength
        ypos = self.IsOutput()*prinlength
        return self.GetOverlapMatrix()[xpos:xpos+prinlength, ypos:ypos+prinlength]

    def SetScatteringSlices(self, slices):
        self.GetLeadIndices().SetScatteringSlices(slices)
        self.GetCouplingIndices().SetScatteringSlices(slices)

class LeftLeadHamiltonianMatrix(LeadHamiltonianMatrix):
    """ Given the bulkhamiltonian this class returns the hamiltonian and
        overlapmatrix for a left lead."""
    def __init__(self,
                 principallayer,
                 hamiltonianmatrix=None,
                 overlapmatrix=None,
                 filename=None,
                 leadindices=None,
                 couplingindices=None):
        LeadHamiltonianMatrix.__init__(self,
                                       principallayer,
                                       hamiltonianmatrix,
                                       overlapmatrix,
                                       filename,
                                       leadindices,
                                       couplingindices,
                                       leadtype="left")

    # Methods used to define and call the Hamiltonian and overlapmatrix
    # for the leftlead.
    def GetH_l(self):
        #print "Warning: The Method GetH_l in LeftLeadHamiltonianMatrix is Deprecated"
        return self.GetH()

    def GetS_l(self):
        #print "Warning: The Method GetS_l in LeftLeadHamiltonianMatrix is Deprecated"
        return self.GetS()


    # The interaction between two principallayers in the lead.

    def GetT_l(self):
        #print "Warning: The Method GetT_r in RightLeadHamiltonianMatrix is Deprecated"
        return self.GetT()

    def GetInteractionS_l(self):
        #print "Warning: The Method GetInteractionsS_r in RightLeadHamiltonianMatrix is Deprecated"
        return self.GetInteractionS()


class RightLeadHamiltonianMatrix(LeadHamiltonianMatrix):
    """ Given the bulkhamiltonian this class returns the hamiltonian and
        overlapmatrix for a right lead."""
    def __init__(self,
                 principallayer,
                 hamiltonianmatrix=None,
                 overlapmatrix=None,
                 filename=None,
                 leadindices=None,
                 couplingindices=None):
        """Methods used to define and call the Hamiltonian and overlapmatrix for the rightlead."""
        LeadHamiltonianMatrix.__init__(self,
                                       principallayer,
                                       hamiltonianmatrix,
                                       overlapmatrix,
                                       filename,
                                       leadindices,
                                       couplingindices,
                                       leadtype="right")



    def GetH_r(self):
        #print "Warning: The Method GetS_r in RightLeadHamiltonianMatrix is Deprecated"
        return self.GetH()

    def GetS_r(self):
        #print "Warning: The Method GetS_r in RightLeadHamiltonianMatrix is Deprecated"
        return self.GetS()

    # The interaction between two principallayers in the lead.

    def GetT_r(self):
        #print "Warning: The Method GetT_r in RightLeadHamiltonianMatrix is Deprecated"
        return self.GetT()

    def GetInteractionS_r(self):
        #print "Warning: The Method GetInteractionsS_r in RightLeadHamiltonianMatrix is Deprecated"
        return self.GetInteractionS()
