# Written by Mikkel Bollinger email: mbolling@fysik.dtu.dk
"""Module containing additional methods for NumPy arrays

The methods in this module are used by 'Structures.Grid' and classes related
to it by inheritance.
"""
import copy
from numpy import zeros


def TranslateAlongAxis0NonPeriodic(array, translation, mytype):
    """Optimized method for translating along axis o"""
    #import Numeric
    if translation == 0:
        return copy.copy(array)
    newarray = zeros(array.shape, mytype)
    if translation > 0:
        newarray[translation:] = array[:-translation]
    else:
        newarray[:translation] = array[-translation:]
    return newarray

def TranslateAlongAxis0(array, translation):
    """Optimized method for translating along axis o"""
    #from Numeric import concatenate, take
    if translation == 0:
        return copy.copy(array)
    newarray = copy.copy(array)
    newarray[:translation] = array[-translation:]
    newarray[translation:] = array[:-translation]
    return newarray
