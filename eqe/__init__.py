# Copyright 2024, Yachay Tech University

"""External Quantum Efficiency"""

from eqe.externalquantumefficiency import ExternalQuantumEfficiency
from eqe.externalquantumefficiency import main

__all__ = ['ExternalQuantumEfficiency', 'main']
__version__ = '24.01'
