#!/usr/bin/env python

"""Generic Class for Executing Tests of
ExternalQuantumEfficiency class"""

from __future__ import print_function

from argparse import ArgumentParser

from numpy import array

from gpaw.test import equal
from eqe import ExternalQuantumEfficiency

class TestEQE():
    """Class for running generic tests on
    ExternalQuantumEfficiencies class"""
    def __init__(self, filename, mode, results, pdir, qdir, error=1e-8):
        """Generic test of ExternalQuantumEfficiency class
        filename	GPAW filename, None for TightBinding test
        mode		Test mode, one of 'sparse', 'full', 'efficient'
        mode='sparse'	Calculate EQE only for frequencies in results
        mode='full'	Calculate EQE for a default dense frequency mesh
        mode='efficient' Calculate EQE only for energies below the VBM
        results		Dictionary of results to be tested against
    			results[frequency] = efficiency
        pdir		propogation direction
        qdir		polarization direction
        error		Error tolerance in eqe calculations"""
        tightbinding = filename is None
        self.mode_dict = ['sparse', 'full', 'efficient']
        assert mode in  self.mode_dict
        self.mode = mode
        efficient = mode == 'efficient'
        # in sparse mode load frequencies
        if mode == 'sparse':
            self.frequencies = array(list(results.keys()))
        else:
            self.frequencies=None
        assert pdir in [0, 1, 2]
        assert qdir in [0, 1, 2]
        self.error = error
        self.eqe = ExternalQuantumEfficiency(filename=filename,
                                             tightbinding=tightbinding,
                                             efficient=efficient,
                                             pdir=pdir,
                                             qdir=qdir,
                                             frequencies=self.frequencies)
        print(self.eqe)
        self.check_results(results)

    def check_results(self, results, spin=0):
        """Check results from ExternalQuantumEfficiency
        results[frequencies] = efficiencies
        spin	spin channel, in [0,1]"""
        eqe = self.eqe
        efficiencies = eqe.get_efficiencies(spin=spin)
        frequencies = self.frequencies
        if spin:
            print('spin =', spin)
        if frequencies is None:
            frequencies = eqe.get_frequencies()
        results_frequencies = array(list(results.keys()))
        for frequency in results_frequencies:
            eqe = efficiencies[abs(frequencies-frequency).argmin()]
            print("    results["+str(frequency)+"] = "+str(eqe))
            equal(eqe, results[frequency], self.error)
        print('    emax =', frequencies[efficiencies.argmax()])

def read_arguments():
    """Input Argument Parsing"""
    parser = ArgumentParser()
    parser.add_argument('-eff', '--efficient',
                        help='optimize calculations for efficiency (%(default)s)',
                        action='store_true')
    parser.add_argument('-f', '--full',
                        help='full calculation (%(default)s)',
                        action='store_true')
    parser.add_argument('-err', '--error',
                        help='error tolerancy in test (%(default)s)',
                        type=float, default=1e-8)
    pargs = parser.parse_args()
    if pargs.full:
        pargs.mode = 'full'
        if pargs.efficient:
            pargs.mode = 'efficient'
    else:
        pargs.mode = 'sparse'
    return pargs
