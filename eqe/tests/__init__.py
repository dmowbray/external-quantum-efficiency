# Copyright 2024, Yachay Tech University

"""External Quantum Efficiency Tests"""

from eqe.tests.test_eqe import TestEQE, read_arguments

__all__ = ['TestEQE', 'read_arguments']
__version__ = '24.01'
